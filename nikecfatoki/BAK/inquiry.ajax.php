<?php


define ('OUTPUT', 'AJAX_JSON');







 
/**
 * inoculate against hack attempts which waste CPU cycles
 */
$contaminated = (isset($_FILES['GLOBALS']) || isset($_REQUEST['GLOBALS'])) ? true : false;
$paramsToAvoid = array('GLOBALS', '_COOKIE', '_ENV', '_FILES', '_GET', '_POST', '_REQUEST', '_SERVER', '_SESSION', 'HTTP_COOKIE_VARS', 'HTTP_ENV_VARS', 'HTTP_GET_VARS', 'HTTP_POST_VARS', 'HTTP_POST_FILES', 'HTTP_RAW_POST_DATA', 'HTTP_SERVER_VARS', 'HTTP_SESSION_VARS');
$paramsToAvoid[] = 'autoLoadConfig';
$paramsToAvoid[] = 'mosConfig_absolute_path';
$paramsToAvoid[] = 'hash';
$paramsToAvoid[] = 'main';
foreach($paramsToAvoid as $key) {
  if (isset($_GET[$key]) || isset($_POST[$key]) || isset($_COOKIE[$key])) {
    $contaminated = true;
    break;
  }
}

$paramsToCheck = array('fullname', 'email', 'subject', 'about');


if (!$contaminated) {
  foreach($paramsToCheck as $key) {
    if (isset($_GET[$key]) && !is_array($_GET[$key])) {
      if (substr($_GET[$key], 0, 4) == 'http' || strstr($_GET[$key], '//')) {
        $contaminated = true;
        break;
      }
      if (isset($_GET[$key]) && strlen($_GET[$key]) > 43) {
        $contaminated = true;
        break;
      }
    }
  }
}
unset($paramsToCheck, $paramsToAvoid, $key);
if ($contaminated)
{
  header('HTTP/1.1 406 Not Acceptable');
  exit(0);
}
unset($contaminated);
/* *** END OF INNOCULATION *** */







// Check for direct calls to AJAX scripts...
if (defined('OUTPUT') && (OUTPUT == 'JSON' || OUTPUT == 'AJAX_JSON' || OUTPUT == 'AJAX_HTML')) {
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		// This an ajax request. Do nothing.
	 } else {
		header('Location: http://' . $_SERVER['HTTP_HOST']);
		exit();
	 }
}



header('Content-type: application/x-json');



// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') die('x_x');
	



require_once ('includes/config.php');

require_once(SHARED_INC_PATH.'recaptcha/recaptchalib.php');


/////////////////////// RECAPTCHA ////////////////////


# the response from reCAPTCHA
$resp = null;
# the error code from reCAPTCHA, if any
// $error = null;

# was there a reCAPTCHA response?
if ($_POST["recaptcha_response_field"]) {
        $resp = recaptcha_check_answer (RECAPTCHA_PRIVATE_KEY,
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);
}








// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
                                                array(	
                                                                // Full Name
                                                                'fullname'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z0-9-_ ]{2,150}$/')
                                                                                                                ), 
                                                                'email'	=>	FILTER_VALIDATE_EMAIL, 
                                                                'subject'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z0-9-_ ]{2,200}$/')
                                                                                                                ), 
                                                    
                                                                /*
                                                    
                                                                'captcha'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '!^[a-zA-Z0-9\/\\- ]{2,100}$!')
                                                                                                                ), 
                                                                 
                                                                 */
                                                                'about'	=>	FILTER_SANITIZE_STRING

                                                                )
                                                );



// FAILED ITEMS ARRAY 


$_FailedItemLabels = array(	
                        'fullname'	=>	'Your Full Name', 
                        'email'	=>	'Your Email', 
                        'subject'	=>	'Subject', 
                        'captcha'	=>	'Captcha', 
                        'about'	=>	'Question / Comment'

                        );



// OUTPUT...
 $json_reply = array (
                 "status" => 'ok', 
                 "message" => '<p>Your request has been sent. Thank you for contacting us.</p>', 
                 "failedValidation" => FALSE
                 
                 );
 
 
 
 

// Check post data
 
 if (empty($_CLEAN['about'])) $_CLEAN['about'] = FALSE;
 
 
 if (in_array(FALSE, $_CLEAN) || !$resp->is_valid) {

    $json_reply['status'] = 'ok';
    $json_reply['message'] = '<p>Please fill out the following correctly:<br /><br />';
    $json_reply['failedValidation'] = TRUE;
    
} 








$_i = 1;


foreach ($_CLEAN as $k => $v) if ($v === FALSE) $json_reply['message'] .= (($_i++) . '. ' . $_FailedItemLabels[$k] . '<br />');


if (!$resp->is_valid) $json_reply['message'] .= (($_i++) . '. Recaptcha<br />');



if ($json_reply['failedValidation']) {
    
    $json_reply['message'] .= '</p>';
    //$json_reply['recaptchaHTML'] = recaptcha_get_html(RECAPTCHA_PUBLIC_KEY);
    echo json_encode($json_reply);

    exit ();
    
}













// Load template...
$template = Cataleya_Class_Twiggy::loadTemplate('inquiry.email.html.twig');

$twig_vars = array ();


$twig_vars['email'] = array (
    'subject'  =>  ('Inquiry by ' . $_CLEAN['firstname']), 
    'message' =>  '', 
    'Customer' => array (
        'fullname'  =>  $_CLEAN['fullname'], 
        'email' =>  $_CLEAN['email'], 
        'subject' =>  $_CLEAN['subject'], 
        'about' =>  $_CLEAN['about']
        )
);



$twig_vars['header_title'] = $twig_vars['inline_title'] = 'Inquiry';
$twig_vars['title_icon'] = BASE_URL . 'images/notification-icons/email-50x50.png';


$twig_vars['contact_email'] = EMAIL_INQUIRIES;


// Setup transport
$transport = Swift_SmtpTransport::newInstance()
                                                ->setHost(EMAIL_SERVER_HOST)
                                                ->setUsername(EMAIL_USER)
                                                ->setPassword(EMAIL_PASS)
                                                ->setPort(26);




// Get mailer													
$mailer = Swift_Mailer::newInstance($transport);

$message = Swift_Message::newInstance();




$message->setSubject('Inquiry by ' . $_CLEAN['fullname']);
$message->setFrom(array(EMAIL_HOST=> 'Mailer' ));
$message->setSender(EMAIL_HOST);
$message->setTo(array( EMAIL_INQUIRIES => 'Nike Campbell-Fatoki' ));
$message->setReplyTo($_CLEAN['email']);




// New email


$textBody = "Inquiry by: " . $_CLEAN['fullname'] . "! \n\n";
$textBody .= "Email: " . $_CLEAN['email'] . "\n\n";
$textBody .= "Subject: " . $_CLEAN['subject'] . "\n\n";
$textBody .= "Question/Comment: \n\n";
$textBody .= "------------------------------- \n\n" . $_CLEAN['about'] . "\n\n";
        
$message->setBody($textBody);
$message->addPart($template->render($twig_vars), 'text/html');
$message->setPriority(3);


               

try {
    $result = $mailer->send($message);

} catch (Swift_SwiftException $e) {
    $result = 0;
}




 

if ($result === 0) {
     $json_reply['status'] = 'error';
     $json_reply['message'] = '<p>Your inquiry could not be sent.</p>';
} 



    // $json_reply['recaptchaHTML'] = recaptcha_get_html(RECAPTCHA_PUBLIC_KEY);
    echo json_encode($json_reply);

exit ();




?>
