<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>


<div class="post" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="content">


        <?php if ( is_single() ) : ?>
        <h3 class="entry-title"><?php the_title(); ?></h3>
        <?php else : ?>
        <strong><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong>
	<?php endif; // is_single() ?>
        
        <br>

        <i><?php the_date(); ?></i>

        <br>
        
        <?php the_content( 'Continue reading ...' ); ?>
        
        <br><br>

    </div>
</div>
                            
           