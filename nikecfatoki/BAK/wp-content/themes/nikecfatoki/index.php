
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">    
        
        
        
        
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php bloginfo('name'); ?><?php wp_title (); ?></title>



        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        

        
        
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/home.css" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />



        <!-- JAVASCRIPT -->


        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/lib/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/hashgrid.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/h5bp/plugins.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/h5bp/main.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/lib/modernizr-2.6.2.min.js"></script> 



        
        <style type="text/css" media="screen">
            


        </style>

    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        


        <div class="container clearfix">

        <?php get_header(); ?>

        
        <div class="content">
            <div class="wrap" id="main-content-wrap" >
                
                
                <div class="inner-content-wrap">
                    
                    
                    <div class="le-livre">
                        &nbsp;
                    </div>
                    
                    <div id="splash-001" class="tile-content-wrapper" >
                    <ul class="slide-strip" >


                        <li class="slide" >

                        <div class="slide-content-wrapper" style="background: none'); " >
                            
                            <p>
                                &ldquo;You must run now Amelia. The kingdom is about to fall. We may fall with it but you cannot. 
                                You are all I have fought for. To see you run free is for your mother to finally run free; 
                                for me to finally be free. You must go now!&rdquo;
                            </p>
                        </div>

                        </li>
                        
                        
                        <li class="slide" >

                        <div class="slide-content-wrapper" style="background: none'); " >
                            <p class="quote">
                                “A highly competent contribution to the growing genre of popular 
                                historical fiction in Africa."
                            </p>
                            
                            <p class="quotee">
                                - Sefi Atta, Author of A Bit of A Difference, Swallow, 
                                Everything Good Will Come. Winner of Wole Soyinka Prize 
                                for Literature in Africa
                            </p>
                            
                        </div>

                        </li>


                    </ul>
                    </div>
                    
                </div>
                
                
                <div class="announcements">
                    <?php
                    
                    $idObj = get_category_by_slug('announcements'); 
                            
                    $args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 1);
                    $myposts = get_posts( $args );
                    
                    if ( !empty($myposts) ) : ?>

                            <?php /* The loop */

                            
                            ?>
                            <?php foreach( $myposts as $post ) : setup_postdata($post); ?>
                                    <?php get_template_part( 'content', 'latest' ); break; ?>
                            <?php endforeach; ?>


                    <?php else : ?>
                            <?php get_template_part( 'content', 'none' ); ?>
                    <?php endif; ?>
                </div>

                
                <div class="inner-footer">
                    
                    <div class="ankara-02" id="footer-ankara-disc">
                        &nbsp;
                    </div>
                    
                    
                    <div class="inner-footer-wrap">
                        
                        <p class="footer-quote">
                            Your life is a book. Some have written half-way, some, three-quarter way. 
                            You have the opportunity to write and rewrite the book of your life. Make 
                            sure you write a masterpiece.
                        </p>

                        <ul id="social-anchors">
                            <a target="_blank" href="http://www.facebook.com/threadofgoldbeads"><li class="fb-anchor">&nbsp;</li></a>
                            <a target="_blank" href="https://twitter.com/nikecfatoki"><li class="twitter-anchor">&nbsp;</li></a>
                            <a target="_blank" href="http://www.googleplus.com"><li class="google-anchor last-child">&nbsp;</li></a>

                        </ul> 
                        
                        
                    </div>

                    
                </div>
            </div>
        
        </div>
        
        <?php // get_sidebar(); ?>
        <?php get_footer(); ?>
        
        
            
        </div>

        <script type="text/javascript">
            
        $(window).load(function() {

                makeSlideshow($('#splash-001').find('.slide-strip'), {duration: 200, interval: 8000, transition: 'fade'});
                                

            });
            
            
        
            

            
        </script>
        
        
    </body>
</html>
