<?php
/*
Template Name: Custom Page Template - Home
*/
?>



<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">    
        
        
        
        
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php bloginfo('name'); ?><?php wp_title (); ?></title>



        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        

        
        
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />



        <!-- JAVASCRIPT -->


        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/lib/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/hashgrid.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/h5bp/plugins.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/h5bp/main.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/lib/modernizr-2.6.2.min.js"></script> 



        
        <style type="text/css" media="screen">
            
            #main-content-wrap {
    
                height: 1700px;
                width: 800px;


                background: #eee url('<?php bloginfo('template_url') ?>/images/001c.jpg') no-repeat;
                background-position: top center;
                background-size: contain;
                
                border-radius:7px;	
                -moz-border-radius:7px;	
                -webkit-border-radius:7px;

                margin: 20px auto; /* the auto value on the sides, coupled with the width, centers the layout */
            }




        </style>

    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        


        <div class="container clearfix">

        <?php // get_header(); ?>

        
        <div class="content">
            <div class="wrap" id="main-content-wrap" >
                

                
                
                
                <div id="inner-content-wrap" >

                
                <br class="clearfix" />
                </div>
                

            </div>
        
        </div>
        
        <?php get_sidebar(); ?>
        <?php get_footer(); ?>
        
        
            
        </div>

        <script type="text/javascript">
            
        $(window).load(function() {


            });
            
            
        
            

            
        </script>
        
        
    </body>
</html>
