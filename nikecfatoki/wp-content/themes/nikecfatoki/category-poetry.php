<?php
/*
Template Name: Custom Page Template - Video and Audio
*/
?>

        <?php get_header(); ?>


        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/category.css" />
                <style type="text/css" media="screen">
                    .inner-content-wrap {
                        width: 85%;
                        margin: 0 auto;
                        margin-bottom: 20px;

                        font-size: 100%;
                    }


                    #footer-ankara-disc {
                        background:url(<?php bloginfo('template_url') ?>/images/ankara-05b.png);
                    }





                </style>


        <div class="content">
            <div class="wrap" id="main-content-wrap" >


                <div class="inner-content-wrap clearfix">

                <?php get_template_part( 'cat-title' ); ?>

		<?php

                //The Query

                //$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                //query_posts('posts_per_page=2');



                if ( have_posts() ) : ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'no-date' ); ?>
			<?php endwhile;


                        get_paging_nav();

                        ?>



                    <?php else : ?>
                            <?php get_template_part( 'content', 'none' ); ?>
                    <?php endif; ?>


                    <?php

                    //Reset Query
                    //wp_reset_query();

                    ?>
                </div>









                <div class="inner-footer">

                    <div class="ankara-02" id="footer-ankara-disc">
                        &nbsp;
                    </div>


                    <div class="inner-footer-wrap">

                        <?php get_template_part( 'footer-quote' ); ?>

                        <?php get_template_part( 'social-links' ); ?>



                    </div>


                </div>
            </div>

        </div>

        <?php // get_sidebar(); ?>
        <?php get_footer(); ?>
