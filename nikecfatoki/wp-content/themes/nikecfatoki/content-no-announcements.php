<?php
/**
 * The template for displaying a "No posts found" message.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>




<div class="post" id="post-0">

    <div class="content">

        <br />


        <h3 class="entry-title">No Items</h3>
        
        <br>


        <br>
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

	<p><?php printf( __( 'There are no items here just yet.', 'nikecfatoki' ), admin_url( 'post-new.php' ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

	<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with different keywords.', 'nikecfatoki' ); ?></p>
	<?php // get_search_form(); ?>

	<?php else : ?>

	<p><?php _e( 'There are no items here just yet. ', 'nikecfatoki' ); ?></p>
	<?php // get_search_form(); ?>

	<?php endif; ?>
        
        <br><br>

    </div>
</div>
                            
    