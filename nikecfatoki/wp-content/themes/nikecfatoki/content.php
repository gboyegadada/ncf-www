<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Nike Campbell-Fatoki
 * 
 */
?>


<div class="post clearfix" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="content">


        <h3 class="entry-title">
            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
        </h3>

        <!--
        <i><?= get_the_date(); ?></i>

        <br>
        -->
        
        <?php the_content( 'Continue reading ...' ); ?>
        
        <br><br>
        <?php edit_post_link( 'Edit This', '', '<br /><br />' ); ?>

    </div>
</div>
                            
           