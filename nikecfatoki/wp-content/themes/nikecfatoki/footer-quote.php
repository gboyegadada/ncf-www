<p class="footer-quote">
    <?php

    $idObj = get_category_by_slug('quotes'); 

    $args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 1, 'orderby' => 'rand');
    $myposts = get_posts( $args );

    if ( !empty($myposts) ) : ?>

            <?php /* The loop */


            ?>
            <?php foreach( $myposts as $post ) : setup_postdata($post); ?>

                    <?= strip_tags(get_the_content( '' ), ''); ?>

            <?php endforeach; ?>

    <?php endif; ?>
</p>