<?php


/*
 * [ readFolder ]
 * ___________________________________________
 *
 * Read contents of a folder and return filenames in an array.
 *
 *
 */


function readFolder ($_PATH = NULL, $_FILTER = '*')
{
    if (!is_string($_FILTER) || empty($_PATH) || !is_string($_PATH) || !file_exists($_PATH)) return array ();


    $_FILTER = preg_replace('/([^a-zA-Z0-9-_]+)/i', '', $_FILTER);
    if (strlen($_FILTER) > 20) return array ();

    if (empty($_FILTER)) $_FILTER = '*';



    $_files = array ();
    $dh  = scandir($_PATH, 0);

    foreach ($dh as $filename) {
        if (preg_match('/^([a-zA-Z0-9-_]+)(\.' . $_FILTER . ')$/i', $filename)) $_files[] = $filename;
    }


    return  empty ($_files) ? array ('toystory.jpg') : $_files;
}











// parses and return content of file name passed to it...


function read_flat_file ($file_path)
{

if (!file_exists($file_path)) {return array (); }


$flat_file = array();



$buffer = file($file_path);


		foreach ($buffer as $line)
		{
		$line = ereg_replace("([\n\r]+)", "", $line);
		$row_buffer = array();

		$cols_buffer = explode ("|", $line);

			foreach ($cols_buffer as $col)
			{
			list ($field, $value) = explode ("=", $col);

			$row_buffer[$field] = $value;


			}

		$flat_file[count ($flat_file)] = $row_buffer;
		// array_push( $flat_file, $row_buffer);


		}



return $flat_file;
}




function write_flat_file ($file_path, $flat_file)
{
$data_string = '';



foreach ($flat_file as $row)
	{
	$string_buffer = array();

	foreach ($row as $field=>$value)
		{
		array_push( $string_buffer, "$field=$value");
		}

	$data_string .= implode ("|", $string_buffer) . "\n";


	}

				$data_string = ereg_replace("([\n]+)$", "", $data_string);

				$new_file = fopen($file_path, 'w');
				fwrite($new_file, $data_string);
				fclose($new_file);

return true;

}




















if ( ! function_exists( 'fpp_post_nav' ) ) :
/**
 * Displays navigation to next/previous post when applicable.
*
* @since FPP 1.0
*
* @return void
*/
function fpp_post_nav() {
	global $post;

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Post navigation', 'twentythirteen' ); ?></h1>
		<div class="nav-links">

			<?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'twentythirteen' ), TRUE ); ?>
			<?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'twentythirteen' ), TRUE ); ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;






/**
 * Displays navigation to next/previous set of posts when applicable.
 *
 * @since 1.0
 *
 * @return void
 */
function get_paging_nav() {
	global $wp_query;

	// Don't print empty markup if there's only one page.
	if ( $wp_query->max_num_pages < 2 )
		return;
	?>
	<nav class="navigation paging-navigation" role="navigation">
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( 'older' ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( 'newer' ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}



















/**
 * Display navigation to next/previous post when applicable.
*
* @since Twenty Thirteen 1.0
*
* @return void
*/
function get_post_nav() {
	global $post;

	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( true, '', true );
	$next     = get_adjacent_post( true, '', false );

	if ( ! $next && ! $previous )
		return;
	?>
	<nav class="navigation post-navigation" role="navigation">
		<div class="nav-links clearfix">

			<?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'nikecfatoki' ), TRUE ); ?>
			<?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'nikecfatoki' ), TRUE ); ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}



/**
 * Get the absolute filesystem path to the root of the WordPress installation
 *
 * @since 1.5.0
 *
 * @uses get_option
 * @return string Full filesystem path to the root of the WordPress installation
 */
function get_my_home_path() {
	$home = get_option( 'home' );
	$siteurl = get_option( 'siteurl' );
	if ( ! empty( $home ) && 0 !== strcasecmp( $home, $siteurl ) ) {
		$wp_path_rel_to_home = str_ireplace( $home, '', $siteurl ); /* $siteurl - $home */
		$pos = strripos( str_replace( '\\', '/', $_SERVER['SCRIPT_FILENAME'] ), trailingslashit( $wp_path_rel_to_home ) );
		$home_path = substr( $_SERVER['SCRIPT_FILENAME'], 0, $pos );
		$home_path = trailingslashit( $home_path );
	} else {
		$home_path = ABSPATH;
	}

	return str_replace( '\\', '/', $home_path );
}




?>
