

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <?php get_template_part( 'meta' ); ?>

        <title><?php bloginfo('name'); ?>  &#8212;  <?php the_title (''); ?></title>



        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->





        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/home.css" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />



        <!-- JAVASCRIPT -->


        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/lib/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/hashgrid.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/h5bp/plugins.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/h5bp/main.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/lib/modernizr-2.6.2.min.js"></script>




        <style type="text/css" media="screen">



        </style>
        <?php wp_head(); ?>

    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->



        <div class="container clearfix">

<div class="header">


        <!--



            <div class="search-form-wrap">
                <?php // get_search_form(); ?>
            </div>

        -->



        <div id="primary-nav-wrapper" class="header-row wrap clearfix">

            <a href="http://www.nikecfatoki.com/" >
                <div class="main-logo">
                    &nbsp;
                </div>
            </a>



            <div class="aujourd-hui">
                <?php


                setlocale(LC_TIME, "fr_FR.UTF-8");

                echo strftime("%d %b %Y", time());

                ?>.
            </div>



            <div class="ankara-01">
                &nbsp;
            </div>

            <ul id="primary-nav" class="clearfix">


                <a href="http://www.nikecfatoki.com/home/" ><li class="nav-home">Home</li></a>



                <a href="http://www.nikecfatoki.com/about-the-author-2/" ><li class="nav-about" >About</li></a>



                <a href="http://www.nikecfatoki.com/category/poetry/" ><li class="nav-poetry">Poetry</li></a>



                <a href="http://www.nikecfatoki.com/category/press/" ><li class="nav-press">Press</li></a>



                <a href="http://www.nikecfatoki.com/category/video-audio/" ><li class="nav-video">Video</li></a>



                <a href="http://www.nikecfatoki.com/blog/" ><li class="nav-blog">Blog</li></a>



                <a href="http://www.nikecfatoki.com/category/events/" ><li class="nav-events">Events</li></a>


                <a href="http://www.nikecfatoki.com/gallery/" ><li class="nav-gallery">Gallery</li></a>


                <a href="http://www.nikecfatoki.com/contact-2/" ><li class="nav-contact">Contact</li></a>

            </ul>




        </div>

</div>
