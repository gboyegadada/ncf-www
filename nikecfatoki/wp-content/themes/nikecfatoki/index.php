<?php
/*
Template Name: Custom Page Template - Home
*/
?>


<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/home.css" />

        <?php get_header(); ?>


        <div class="content">
            <div class="wrap" id="main-content-wrap" >


                <div class="inner-content-wrap clearfix">



                    <a class="book-page-anchor" href="<?= home_url('thread-of-gold-beads'); ?>">
                    <div class="le-livre">
                        &nbsp;
                    </div>
                    </a>


                    <div class="now-available-at">
                        <a class="call-to-action-anchor" href="<?= home_url('thread-of-gold-beads'); ?>"><span>NOW AVAILABLE</span> <br />ON <div class="amazon-button-color">&nbsp;</div> AND IN SELECTED STORES</a>
                    </div>





                    <div id="splash-001" class="tile-content-wrapper" >
                    <ul class="slide-strip" >


                        <li class="slide" >

                        <div class="slide-content-wrapper" style="background: none'); " >

                            <p>
                                &ldquo;You must run now Amelia. The kingdom is about to fall. We may fall with it but you cannot.
                                You are all I have fought for. To see you run free is for your mother to finally run free;
                                for me to finally be free. You must go now!&rdquo;

                                <br /><br />

                                - Thread of Gold Beads
                            </p>
                        </div>

                        </li>


                        <li class="slide" >

                        <div class="slide-content-wrapper" style="background: none'); " >
                            <p class="quote">
                                “A highly competent contribution to the growing genre of popular
                                historical fiction in Africa."
                            </p>

                            <p class="quotee">
                                - Sefi Atta, Author of A Bit of A Difference, Swallow,
                                Everything Good Will Come. Winner of Wole Soyinka Prize
                                for Literature in Africa
                            </p>

                        </div>

                        </li>


                    </ul>
                    </div>






                </div>



                <div class="announcements">

                    <h3>Announcements</h3>
                    <?php

                    $idObj = get_category_by_slug('announcements');

                    $args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 1);
                    $myposts = get_posts( $args );

                    if ( !empty($myposts) ) : ?>

                            <?php /* The loop */


                            ?>
                            <?php foreach( $myposts as $post ) : setup_postdata($post); ?>
                                    <?php get_template_part( 'content', 'latest' ); break; ?>
                            <?php endforeach; ?>


                    <?php else : ?>
                            <?php get_template_part( 'content', 'no-announcements' ); ?>
                    <?php endif; ?>
                </div>


                <div class="inner-footer">




                    <div id="tree">
                        &nbsp;
                    </div>



                    <div class="ankara-02" id="footer-ankara-disc">
                        &nbsp;
                    </div>


                    <div class="inner-footer-wrap">

                        <p class="footer-quote">
                            Your life is a book. Some have written half-way, some, three-quarter way.
                            You have the opportunity to write and rewrite the book of your life. Make
                            sure you write a masterpiece.
                        </p>

                        <ul id="social-anchors">
                            <a target="_blank" href="http://www.facebook.com/threadofgoldbeads"><li class="fb-anchor">&nbsp;</li></a>
                            <a target="_blank" href="https://twitter.com/nikecfatoki"><li class="twitter-anchor">&nbsp;</li></a>
                            <a target="_blank" href="http://www.googleplus.com"><li class="google-anchor last-child">&nbsp;</li></a>

                        </ul>


                    </div>


                </div>
            </div>

        </div>

        <?php // get_sidebar(); ?>


                <script type="text/javascript">

                $(window).load(function() {

                        makeSlideshow($('#splash-001').find('.slide-strip'), {duration: 200, interval: 8000, transition: 'fade'});


                    });






                </script>

                
        <?php get_footer(); ?>
