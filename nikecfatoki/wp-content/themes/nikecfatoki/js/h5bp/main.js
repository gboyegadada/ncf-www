



var 

_frame_index = [], 
max_frame_rounds = 0, 

activeFrame = 1;






$('document').ready(function () {


        // Set max number of frames
        max_frame_rounds = $('#inner-content-wrap > .content-frame').length;
        
        $('#main-content-wrap').css({height: $('#inner-content-wrap > .content-frame:first').css('height')});
        $('#inner-content-wrap').css({width: (max_frame_rounds*750)});
        

});






    // FUNCTIONS FOR SUB_PANE NAVIGATOR ....

    function nextPane () {  
        var _frame = activeFrame+1;
        return navigateToFrame (_frame);
    }



    function prevPane () {

        var _frame = activeFrame-1;
        navigateToFrame (_frame);
    }
    
    
    function seekPane (_label) {

        for (var i=0; i < _frame_index.length; i++) {
            if (_label == _frame_index[i]) {
                return navigateToFrame (i+1);
                break;
            }
        }

        return false;
    }




    function navigateToFrame (frame) {

        if (frame-1 >= max_frame_rounds || frame-1 < 0) return false;

            activeFrame = frame;
            frame_rounds = frame-1;

            var slide = -((frame-1) * 750);
            $('#inner-content-wrap').animate({left: slide}, 400, function () {
                $('#main-content-wrap').animate({height: $('#inner-content-wrap > .content-frame').eq(frame-1).css('height')}, 300);
            });
            

            return false;

    }
    
    
