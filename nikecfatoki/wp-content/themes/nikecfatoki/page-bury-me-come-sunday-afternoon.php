<?php
/*
Template Name: Custom Page Template - Book
*/

global $wp_query;

?>


        <?php get_header(); ?>


                <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/le-livre.css" />
        <div class="content">
            <div class="wrap" id="main-content-wrap" >


                <div class="inner-content-wrap clearfix">

                    <div class="le-livre bmcsa">
                        &nbsp;
                    </div>

                    <div id="splash-001" class="tile-content-wrapper" >
                    <ul class="slide-strip" >


                        <li class="slide" >

                          <div class="slide-content-wrapper" style="background: none; " >

                              <p>
                                  <i>&ldquo;In this short story collection, Nike Campbell-Fatoki filters the lives of contemporary Nigerians through a colourful and vivid prism, where past sins come to upset settled lives, where lost lives fuel a campaign for a better future and nothing is as it seems.&rdquo;</i>

                                  <br /><br />

                                  <small>- Quramo Publishing</small>
                              </p>
                          </div>

                        </li>

                        <!--
                        <li class="slide" >

                        <div class="slide-content-wrapper" style="background: none; " >
                            <p class="quote">
                                “A highly competent contribution to the growing genre of popular
                                historical fiction in Africa."
                            </p>

                            <p class="quotee">
                                - Sefi Atta, Author of <i>A Bit of A Difference</i>, <i>Swallow</i>,
                                <i>Everything Good Will Come</i>. Winner of Wole Soyinka Prize
                                for Literature in Africa
                            </p>

                        </div>

                        </li>
                      -->

                    </ul>
                    </div>

                </div>








                <!-- WHERE TO BUY -->


                <div class="online-stores">
                    <div id="paypal-shop">
                        <br />
                        Buy NOW through PayPal <br /><br />
                        <!-- <input type="button" value="add to cart" /> -->


                    <div class="wpsc_product_price">
                      <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" name="hosted_button_id" value="2AZWAL2CEJ73N">
                        <table>
                        <tr><td><input type="hidden" name="on0" value="$16.50 for BMCSA + $6 shipping">$16.50 for BMCSA + $6 shipping</td></tr>
                        <tr><td><input type="hidden" name="os0" maxlength="200"></td></tr>
                        </table>
                        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1" style="border:0!important;">
                        </form>                                                                 <!-- multi currency code -->

                    </div>

                    </div>


                <div id="amazon-shop">
                    <br />
                    Or Buy Now At:<br /><br />
                    <a title="Buy on Kindle" href="https://www.amazon.com/Bury-Me-Come-Sunday-Afternoon-ebook/dp/B01KV6VX7Q" target="_blank"><div class="amazon-kindle-button-color">&nbsp;</div></a>

                    <!--
                    <a title="Buy From Amazon" href="http://www.amazon.com/Thread-Gold-Beads-Nike-Campbell-Fatoki/dp/0988193205/ref=sr_1_1?ie=UTF8&amp;qid=1353952661&amp;sr=8-1&amp;keywords=thread+of+gold+beads" target="_blank"><div class="amazon-button-color">&nbsp;</div></a>
                    <a title="Buy From Amazon UK" href="http://www.amazon.co.uk/Thread-Gold-Beads-Nike-Campbell-Fatoki/dp/0988193205/ref=sr_1_1?ie=UTF8&amp;qid=1353952597&amp;sr=8-1"><div class="amazon-uk-button-color">&nbsp;</div></a>
                    <a title="Create Space" href="https://www.createspace.com/3958444" target="_blank"><img class="size-full wp-image-124 alignleft" src="http://www.nikecfatoki.com/wp-content/uploads/2012/09/logo-csp-no-tm1.png" alt="" width="110" height="52" /></a>
                  -->

                </div>


                </div>







                <div class="physical-stores">
                    <br />

                      Also downloadable on <a title="Okada Books" href="http://www.okadabooks.com" target="_blank"><div class="okadabooks-button-color">&nbsp;</div></a> and sold in Nigeria at:

                        <ul class="physical-stores-list">


                            <li>
                                <h6>ABUJA</h6>
                            </li>



                            <li>
                                Salamander Café Limited<br />
                                5 Bujumbura Street, Off Libreville Street, Off Aminu Kano Crescent, Wuse 2 Abuja
                            </li>








                            <li>
                                <h6>LAGOS</h6>
                            </li>

                                <li>
                                    Patabah Bookstore<br />
                                    B18 Adeniran Ogunsanya Shpping Complex Surulere, Lagos
                                </li>

                                <li>
                                    Terra Kulture<br />
                                    Plot 1376, Tiamiyu Savage Street Victoria Island, Lagos
                                </li>

                                <li>
                                    Laterna Ventures Limited<br />
                                    13, Oko Awo Street, Victoria Island, Lagos
                                </li>
                                <li>
                                    Roving Heights Books Nigeria<br />
                                    2A Okuneye Avenue off Pedro Road, Palm Groove, and Lagos
                                </li>

                                <li>
                                    Glendora Bookshop <br />
                                    Ikeja City Mall Alausa, Ikeja, Lagos
                                </li>

                                <li>
                                    The Reading Corner<br />
                                    Fantasyland, 11 Bayo Kuku Road Ikoyi, Lagos

                                </li>


                                <li>
                                    Jazzhole<br />
                                    168 Awolowo Road Ikoyi, Lagos
                                </li>

                                <li>
                                    Quintessence<br />
                                    Falomo Shopping Centre <br />
                                    Ikoyi, Lagos
                                </li>


                        </ul>

                </div>




                <div class="inner-footer">

                    <div class="ankara-02" id="footer-ankara-disc">
                        &nbsp;
                    </div>


                    <div class="inner-footer-wrap">

                        <?php get_template_part( 'footer-quote' ); ?>

                        <?php get_template_part( 'social-links' ); ?>



                    </div>


                </div>
            </div>

        </div>

        <?php // get_sidebar(); ?>
        <?php get_footer(); ?>
