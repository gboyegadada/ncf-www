<?php
/*
Template Name: Custom Page Template - Contact
*/



require_once (get_my_home_path().'/includes/config.php');

header("X-Content-Security-Policy: allow http://" . DOMAIN . "; report-uri " . DOMAIN . "/csp-report; style-src " . DOMAIN . "; img-src " . DOMAIN . " assets.pinterest.com facebook.com facebook.net; object-src " . DOMAIN . "; script-src " . DOMAIN . " assets.pinterest.com facebook.com facebook.net https://apis.google.com https://platform.twitter.com; xhr-src " . DOMAIN . "; frame-src https://plusone.google.com facebook.com facebook.net https://platform.twitter.com; options inline-script setTimeout setInterval;");


?>



<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <?php get_template_part( 'meta' ); ?>   
        
        
        <title><?php bloginfo('name'); ?>  &#8212;  <?php wp_title(''); ?></title>



        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        

        
        
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/contact.css" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />



        <!-- JAVASCRIPT -->


        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/lib/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/hashgrid.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/h5bp/plugins.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/h5bp/main.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/lib/modernizr-2.6.2.min.js"></script> 
        
        <script src="<?php bloginfo('template_url') ?>/js/contact.js"></script> 



        
        <style type="text/css" media="screen">
            .inner-content-wrap p {
                width: 80%;
                margin: 0 auto;
                margin-bottom: 20px;
                
                font-size: 90%;
            }


        </style>

    </head>
    <body>
        <!--[if lt IE 9]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        


        <div class="container clearfix">

        <?php get_header(); ?>

        
        <div class="content">
            <div class="wrap" id="main-content-wrap" >
                
                
                <div class="inner-content-wrap">
                    
                    <?php get_template_part( 'page-title' ); ?>
                    
                    <div id="contact-form-wrap">
                        
                        <form id="contact-form" class="form-container">

                        <div class="form-fields">


                                <div class="form-title">Full Name</div>
                                <input class="form-field" type="text" name="fullname" maxlength="60" /><br />
                                <div class="form-title">Your Email</div>
                                <input class="form-field" type="text" name="email" maxlength="60" /><br />

                                <div class="form-title">Subject</div>
                                <input class="form-field" type="text" name="subject" maxlength="200" /><br />



                                <div class="form-title">Question/Comment</div>
                                <textarea class="form-field" name="about" rows="6"></textarea><br />

                                <!--
                                <div class="form-title">Please enter the security code</div>
                                <div id="recaptcha-wrap">
                                <?php //recaptcha_get_html(RECAPTCHA_PUBLIC_KEY); ?> 
                                </div>
                                -->
                                
                                <br /> 

                                <div class="submit-container">
                                <input class="submit-button" type="submit" value="submit" />
                                </div>


                        </div>


                        <div class="server-response">
                            <p>

                            </p>


                            <a href='javascript: void(0);' class='submit-button'>back</a>

                        </div>

                        </form>
                        
                    </div>

                </div>
                
                


                
                <div class="inner-footer">
                    
                    <div class="ankara-02" id="footer-ankara-disc">
                        &nbsp;
                    </div>
                    
                    
                    <div class="inner-footer-wrap">
                        
                        <?php get_template_part( 'footer-quote' ); ?>
                        
                        <?php get_template_part( 'social-links' ); ?>
                         
                        
                        
                    </div>

                    
                </div>
            </div>
        
        </div>
        
        <?php // get_sidebar(); ?>
        <?php get_footer(); ?>
        
        
            
        </div>

        <script type="text/javascript">
            
        $(window).load(function() {

                               

            });
            
            
        
            

            
        </script>
        
        
    </body>
</html>
