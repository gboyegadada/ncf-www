<?php
/*
Template Name: Custom Page Template - Gallery
*/


?>




        <?php get_header(); ?>


        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/nivo-themes/default/default.css" />
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/gallery.css" />

        <style type="text/css">
            .inner-content-wrap p {
                width: 80%;
                margin: 0 auto;
                margin-bottom: 20px;

                font-size: 90%;
            }

            #main-content-wrap img {
                /* background-color: #000; */
                border: none;
                /* overflow: hidden; */
                /* padding: 0; */
                margin: 0;
                display: block;
                box-shadow: 0;
                border-radius: 0px;
            }

            .soliloquy-container {
              position: relative!important;
              margin: 0 auto!important;
            }


        </style>

            <div class="content">
                <div class="wrap" id="main-content-wrap" >


                    <div class="inner-content-wrap clearfix">


                        <?php get_template_part( 'page-title' ); ?>

    			<?php while ( have_posts() ) : the_post(); ?>

    				<?php the_content(); ?>


    			<?php endwhile; // end of the loop. ?>

                    </div>





                    <div class="inner-footer">

                        <div class="ankara-02" id="footer-ankara-disc">
                            &nbsp;
                        </div>


                        <div class="inner-footer-wrap">

                            <?php get_template_part( 'footer-quote' ); ?>

                            <?php get_template_part( 'social-links' ); ?>



                        </div>


                    </div>
                </div>

            </div>

            <?php // get_sidebar(); ?>
        <?php get_footer(); ?>
