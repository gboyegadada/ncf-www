<?php
/*
Template Name: Custom Page Template - Home
*/
?>


        <?php get_header(); ?>


        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/home.css" />
        <div class="content">
            <div class="wrap" id="main-content-wrap" >


                <div class="inner-content-wrap clearfix">

                    <div class="welcome-banner-wrap">

                        <a href="<?= home_url('about-the-author-2'); ?>">
                        <div class="welcome-banner">


                            <div id="review-slider" class="tile-content-wrapper clearfix">
                            <ul class="slide-strip" >


                            <li class="slide">
                            <div class="welcome-banner-quote-wrap slide-content-wrapper" >
                                <p class="quote">
                                    <i>&ldquo;These are stories worth telling from a writer worth reading.&rdquo;</i>
                                </p>

                                <p class="quotee">
                                    - Review of <i>Bury Me Come Sunday Afternoon</i> by EC Osondu winner, The Caine Prize for Africa.
                                </p>
                            </div>
                            </li>

                            <li class="slide">
                            <div class="welcome-banner-quote-wrap slide-content-wrapper" >
                                <p class="quote">
                                    <i>&ldquo;A highly competent contribution to the growing genre of popular
                                    historical fiction in Africa.&rdquo;</i>
                                </p>

                                <p class="quotee">
                                    - Sefi Atta, Author of <i>A Bit of Difference</i>, <i>Swallow</i>,
                                    <i>Everything Good Will Come</i>. Winner of Wole Soyinka Prize
                                    for Literature in Africa
                                </p>
                            </div>
                            </li>
                            </ul></div>



                            <div id="ankara-splash">
                                &nbsp;
                            </div>

                            <!--
                            <div id="welcome-to-my-site">
                                <h2>Welcome to my website !</h2>
                            </div>

                            <div id="click-to-learn-about">
                                <h2>Click here to learn more about me ...</h2>
                            </div>
                            -->


                        </div>
                        </a>





                    </div>




                    <div id="shoutbox-slider" class="tile-content-wrapper clearfix">


                    <ul class="slide-strip" >



                            <!-- 02: TOGB PLAY SLIDE

                            <li class="slide" >

                            <div class="slide-content-wrapper" style="background: none; " >
                                <a href="http://togbplay.com" target="_blank">
                                <div class="togb-play-flyer" style="width: 800px; height:536px; margin: 0 auto; background: url('<?php bloginfo('template_url') ?>/images/togb-play-flyer.jpg'); background-repeat: no-repeat; background-size: cover; background-position: top center; ">&nbsp;</div>
                                </a>

                            </div>
                            </li>

                             -->



                            <!-- END 02: TOGB PLAY SLIDE -->





                            <!-- 01: BOOK SLIDE -->

                            <li class="slide" >

                            <div class="slide-content-wrapper" style="background: none; padding-top: 30px; " >

                                        <!--
                                        <div class="synopsis">
                                            <h4>Bury Me Come Sunday Afternoon</h4>
                                            <div class="le-livre bmcsa">&nbsp;</div>
                                            <div class="body">
                                            <p>
                                            In this short story collection, Nike Campbell-Fatoki filters the lives of contemporary Nigerians through a colourful and vivid prism, where past sins come to upset settled lives, where lost lives fuel a campaign for a better future and nothing is as it seems. She explores well-known themes but delves a little deeper, questioning our ideas about people, our impressions and prejudices. Bury Me Come Sunday Afternoon depicts the struggles of a young ambitious and hardworking Nigerian abroad with the same insightful candour as it does the tale of a brilliant but broken woman struggling with mental illness.
                                            </p>
                                            <p>
                                            Nike’s language is precise and direct. Her characters are sharply observant and self-aware even as they battle odds that stack against them. Morals are explored but there is no judgment even when the characters take vengeful and extreme actions. Heroes are created in unlikely scenarios and life as we know it, with more than one surprising twist unfolds in the pages
                                            </p>
                                            <p>
                                            This book is an engaging and breezy read. It peels back layers, it informs, and entertains. Nike Campbell-Fatoki is a writer who will grow in power, providing an empathetic , wise and diverse voice to our typical personal stories.
                                            </p>
                                            <p>
                                            - Quramo Publishing, Out July 2016
                                            </p>
                                            </div>

                                        </div>
                                      -->


                                        <a class="book-page-anchor" href="<?= home_url('bury-me-come-sunday-afternoon'); ?>">
                                        <div class="le-livre bmcsa">
                                            &nbsp;
                                        </div>
                                        </a>


                                        <div class="now-available-at">
                                            <a class="call-to-action-anchor" href="<?= home_url('bury-me-come-sunday-afternoon'); ?>">
                                              <span>NOW AVAILABLE</span>
                                              <br />ON <div class="amazon-button-color">&nbsp;</div> AND IN SELECTED STORES
                                            </a>

                                            <!--
                                          <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                            <input type="hidden" name="cmd" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id" value="2AZWAL2CEJ73N">
                                            <table>
                                            <tr><td><input type="hidden" name="on0" value="$16.50 for BMCSA + $6 shipping">$16.50 for BMCSA + $6 shipping</td></tr>
                                            <tr><td><input type="hidden" name="os0" maxlength="200"></td></tr>
                                            </table>
                                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1" style="border:0!important;">
                                            </form>
                                          -->



                                        </div>




                                        <div id="splash-003" class="tile-content-wrapper book-details-slider" >
                                        <ul class="slide-strip" >


                                            <li class="slide" >

                                            <div class="slide-content-wrapper" style="background: none; " >

                                                <p>
                                                    <i>&ldquo;In this short story collection, Nike Campbell-Fatoki filters the lives of contemporary Nigerians through a colourful and vivid prism, where past sins come to upset settled lives, where lost lives fuel a campaign for a better future and nothing is as it seems.&rdquo;</i>

                                                    <br /><br />

                                                    <small>- Quramo Publishing</small>
                                                </p>
                                            </div>

                                            </li>



                                        </ul>
                                        </div>


                                </div>
                                </li>
                                <!-- END 01: BOOK SLIDE -->



                                <!-- 02: BOOK SLIDE -->

                                <li class="slide" >

                                <div class="slide-content-wrapper" style="background: none; padding-top: 30px; " >

                                        <!--
                                        <div class="synopsis">
                                            <h4>Thread of Gold Beads</h4>

                                            <a class="book-page-anchor" href="<?= home_url('thread-of-gold-beads'); ?>">
                                            <div class="le-livre togb">&nbsp;</div>
                                            </a>

                                            <div class="body">
                                                <p>
                                                    <i>&ldquo;You must run now Amelia. The kingdom is about to fall. We may fall with it but you cannot.
                                                    You are all I have fought for. To see you run free is for your mother to finally run free;
                                                    for me to finally be free. You must go now!&rdquo;</i>

                                                    <br /><br />

                                                    - Thread of Gold Beads
                                                </p>
                                            </div>

                                        </div>
                                      -->



                                            <a class="book-page-anchor" href="<?= home_url('thread-of-gold-beads'); ?>">
                                            <div class="le-livre togb">
                                                &nbsp;
                                            </div>
                                            </a>


                                            <div class="now-available-at">
                                                <a class="call-to-action-anchor" href="<?= home_url('thread-of-gold-beads'); ?>">
                                                  <span>NOW AVAILABLE</span>
                                                  <br />ON <div class="amazon-button-color">&nbsp;</div> AND IN SELECTED STORES
                                                </a>
                                            </div>



                                            <div id="splash-001" class="tile-content-wrapper book-details-slider" >
                                            <ul class="slide-strip" >


                                                <li class="slide" >

                                                <div class="slide-content-wrapper" style="background: none; " >

                                                    <p>
                                                        <i>&ldquo;You must run now Amelia. The kingdom is about to fall. We may fall with it but you cannot.
                                                        You are all I have fought for. To see you run free is for your mother to finally run free;
                                                        for me to finally be free. You must go now!&rdquo;</i>

                                                        <br /><br />

                                                        <small>- Thread of Gold Beads</small>
                                                    </p>
                                                </div>

                                                </li>

                                                <!--
                                                <li class="slide" >

                                                <div class="slide-content-wrapper" style="background: none; " >
                                                    <p class="quote">
                                                        <i>&ldquo;A highly competent contribution to the growing genre of popular
                                                        historical fiction in Africa.&rdquo;</i>
                                                    </p>

                                                    <p class="quotee">
                                                        - Sefi Atta, Author of <i>A Bit of A Difference</i>, <i>Swallow</i>,
                                                        <i>Everything Good Will Come</i>. Winner of Wole Soyinka Prize
                                                        for Literature in Africa
                                                    </p>

                                                </div>

                                                </li>
                                                -->


                                            </ul>
                                            </div>


                                    </div>
                                    </li>
                                    <!-- END 02: BOOK SLIDE -->




                        </ul>
                        </div>





                <div class="announcements">
                   <!-- <h3>Announcements</h3> -->

                    <div id="splash-002" class="tile-content-wrapper" >
                    <ul class="slide-strip" >

                            <?php

                            $idObj = get_category_by_slug('announcements');

                            $args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 5);
                            $myposts = get_posts( $args );

                            if ( !empty($myposts) ) : ?>

                                    <?php /* The loop */


                                    ?>
                                    <?php foreach( $myposts as $post ) : setup_postdata($post); ?>

                                            <li class="slide" >

                                            <div class="slide-content-wrapper" style="background: none; " >
                                            <?php get_template_part( 'content', 'latest' ); ?>
                                            </div>

                                            </li>
                                    <?php endforeach; ?>


                            <?php else : ?>
                                    <?php get_template_part( 'content', 'no-announcements' ); ?>
                            <?php endif; ?>


                    </ul>
                    </div>
                </div>


                <div class="inner-footer">




                    <div id="tree">
                        &nbsp;
                    </div>


                    <!--

                    <div id="indiegogo-wrap">
                        <div class="slog">
                            Be a part of my next project - the Thread of Gold Beads play - coming to the Washington DC area October 4th.
                        </div>

                            <iframe src="https://www.indiegogo.com/project/thread-of-gold-beads-play/embedded" width="222px" height="445px" frameborder="0" scrolling="no"></iframe>


                    </div>

                    -->



                    <div class="ankara-02" id="footer-ankara-disc">
                        &nbsp;
                    </div>


                    <div class="inner-footer-wrap">

                        <?php get_template_part( 'footer-quote' ); ?>

                        <?php get_template_part( 'social-links' ); ?>




                    </div>


                </div>
            </div>

        </div>

        <?php // get_sidebar(); ?>


        <script type="text/javascript">
            var shoutBoxSlider;
        $(window).load(function() {

                new makeSlideshow($('#splash-001 > .slide-strip'), {duration: 200, interval: 8000, transition: 'fade'});
                new makeSlideshow($('#splash-002 > .slide-strip'), {duration: 200, interval: 8000, transition: 'fade'});
                new makeSlideshow($('#splash-003 > .slide-strip'), {duration: 200, interval: 8000, transition: 'fade'});

                var sbSliderDiv = $('#shoutbox-slider');
                shoutBoxSlider = new makeSlideshow($('#shoutbox-slider > .slide-strip'), {duration: 200, interval: 8000, transition: 'fade'});
                new makeSlideshow($('#review-slider > .slide-strip'), {duration: 200, interval: 8000, transition: 'fade'});

                sbSliderDiv
                    .on('mouseenter', function (e) {
                        shoutBoxSlider.pause();
                        sbSliderDiv.find('.synopsis').fadeIn(200);
                    })
                    .on('mouseleave', function (e) {
                        sbSliderDiv.find('.synopsis').fadeOut();
                        shoutBoxSlider.play();
                    });


            });




        </script>

        <?php get_footer(); ?>
