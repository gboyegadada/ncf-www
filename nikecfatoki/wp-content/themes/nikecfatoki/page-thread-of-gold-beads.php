<?php
/*
Template Name: Custom Page Template - Book
*/

global $wp_query;

?>


        <?php get_header(); ?>


                <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/le-livre.css" />
        <div class="content">
            <div class="wrap" id="main-content-wrap" >


                <div class="inner-content-wrap clearfix">

                    <div class="le-livre togb">
                        &nbsp;
                    </div>

                    <div id="splash-001" class="tile-content-wrapper" >
                    <ul class="slide-strip" >


                        <li class="slide" >

                        <div class="slide-content-wrapper" style="background: none; " >

                            <p>
                                <i>&ldquo;You must run now Amelia. The kingdom is about to fall. We may fall with it but you cannot.
                                You are all I have fought for. To see you run free is for your mother to finally run free;
                                for me to finally be free. You must go now!&rdquo;</i>

                                <br /><br />

                                - Thread of Gold Beads
                            </p>
                        </div>

                        </li>


                        <li class="slide" >

                        <div class="slide-content-wrapper" style="background: none; " >
                            <p class="quote">
                                “A highly competent contribution to the growing genre of popular
                                historical fiction in Africa."
                            </p>

                            <p class="quotee">
                                - Sefi Atta, Author of <i>A Bit of A Difference</i>, <i>Swallow</i>,
                                <i>Everything Good Will Come</i>. Winner of Wole Soyinka Prize
                                for Literature in Africa
                            </p>

                        </div>

                        </li>

                    </ul>
                    </div>

                </div>








                <!-- WHERE TO BUY -->


                <div class="online-stores">
                    <div id="paypal-shop">
                        <br />
                        Buy NOW through PayPal <br /><br />
                        <!-- <input type="button" value="add to cart" /> -->


                        <?php

                        // $product = get_post(147);
                        $currency = "&dollar;"; // get_post_meta(147, '_wpsc_currency' , true);
                        $old_price = get_post_meta(147, '_wpsc_price' , true);
                        $special_price = get_post_meta(147, '_wpsc_special_price' , true);

                        number_format($number, $decimals)
                        ?>


                    <div class="wpsc_product_price">

                        <p class="pricedisplay wpsc-product-old-price 147">Old Price: <span class="oldprice" id="old_product_price_147"><?= $currency ?><?= number_format($old_price, 2); ?></span></p>
                        <p class="pricedisplay wpsc-product-price 147">Price: <span class="currentprice pricedisplay 147" id="product_price_147"><?= $currency ?><?= number_format($special_price, 2); ?></span></p>
                        <p class="pricedisplay wpsc-product-you-save product_147">You save: <span class="yousave" id="yousave_147"><span class="pricedisplay"><?= $currency ?><?= number_format($old_price - $special_price, 2) ?></span></span></p>
                                                                                                 <!-- multi currency code -->

                    </div>


                <?php if((get_option('hide_addtocart_button') == 1)) : ?>
                <?php



                echo wpsc_buy_now_button(147);

                else :

                echo wpsc_add_to_cart_button(147);

                ?>

                <?php endif ; ?>















                    </div>

                    <div id="amazon-shop">
                        <br />
                        Or Buy Now At:<br /><br />

                        <a title="Buy From Amazon" href="http://www.amazon.com/Thread-Gold-Beads-Nike-Campbell-Fatoki/dp/0988193205/ref=sr_1_1?ie=UTF8&amp;qid=1353952661&amp;sr=8-1&amp;keywords=thread+of+gold+beads" target="_blank"><div class="amazon-button-color">&nbsp;</div></a>
                        <a title="Buy on Kindle" href="http://www.amazon.com/dp/B00AZ3PBN8" target="_blank"><div class="amazon-kindle-button-color">&nbsp;</div></a>
                        <a title="Buy From Amazon UK" href="http://www.amazon.co.uk/Thread-Gold-Beads-Nike-Campbell-Fatoki/dp/0988193205/ref=sr_1_1?ie=UTF8&amp;qid=1353952597&amp;sr=8-1"><div class="amazon-uk-button-color">&nbsp;</div></a>
                        <a title="Create Space" href="https://www.createspace.com/3958444" target="_blank"><img class="size-full wp-image-124 alignleft" src="http://www.nikecfatoki.com/wp-content/uploads/2012/09/logo-csp-no-tm1.png" alt="" width="110" height="52" /></a>

                    </div>
                </div>







                <div class="physical-stores">
                    <br />
                        <!-- Also sold in Nigeria at <a title="Patabah Bookstore" href="http://www.patabah.com" target="_blank"><div class="patabah-bookstore-button-color">&nbsp;</div></a> -->

                        Also downloadable on <a title="Okada Books" href="http://www.okadabooks.com" target="_blank"><div class="okadabooks-button-color">&nbsp;</div></a> and sold in Nigeria at:


                        <ul class="physical-stores-list">
                            <li>
                                <h6>ABEOKUTA</h6>
                            </li>

                            <li>
                                Mosuro The Booksellers Ltd<br />
                                44 Quarry Road<br />
                                Opposite Anglican School Junction Ibara, Abeokuta
                            </li>

                            <li>
                                Olusegun Obasanjo Presidential Library Bookshop<br />
                                Off Presidential Boulevard<br />
                                Oke-Mosan, Abeokuta
                            </li>



                            <li>
                                <h6>AGO-IWOYE</h6>
                            </li>

                            <li>
                                Olabisi Onabanjo University Bookshop<br />
                                Mini Campus, Olabisi Onabanjo University<br />
                                Ago-Iwoye, Ogun State
                            </li>




                            <li>
                                <h6>IBADAN</h6>
                            </li>

                                <li>
                                WriteHouse Bookshelf<br />
                                NuStreams Conference & Culture Centre<br />
                                Km 110, Iyaganku Road, Off Alalubosa GRA, Ibadan
                                <li />

                                <li>
                                Mosuro The Booksellers Ltd 52 Magazine Road<br />
                                Jericho, Ibadan
                                </li>

                                <li>
                                University of Ibadan Bookshop University of Ibadan<br />
                                Off Agbowo/Ojoo Road, Ibadan
                                </li>

                                <li>
                                Odusote Bookshop<br />
                                68, Obafemi Awolowo Way Oke-Bola, Ibadan
                                </li>






                            <li>
                                <h6>ILE-IFE</h6>
                            </li>

                                <li>
                                    Obafemi Awolowo University Bookshop<br />
                                    Obafemi Awolowo University, Ile-Ife
                                </li>







                            <li>
                                <h6>LAGOS</h6>
                            </li>

                                <li>
                                    Patabah Bookstore<br />
                                    Shoprite<br />
                                    B18 Adeniran Ogunsanya Shpping Complex Surulere, Lagos
                                </li>

                                <li>
                                    LitCafe<br />
                                    E-Centre (Ozone Cinemas)<br />
                                    1-11 Commercial Avenue, Yaba, Lagos
                                </li>

                                <li>
                                    Terra Kulture<br />
                                    Plot 1376, Tiamiyu Savage Street Victoria Island, Lagos
                                </li>

                                <li>
                                    Mosuro The Booksellers Ltd<br />
                                    2 Oweh Street, Near WAEC Yaba, Lagos. Tel: 08063450173
                                </li>

                                <li>
                                    Mosuro The Booksellers Ltd<br />
                                    Business Centre, Pan African University Km 49, Lekki-Epe Expressway, Lagos
                                </li>

                                <li>
                                    Mosuro The Booksellers Ltd<br />
                                    Redeemers’ University (RUN)<br />
                                    Redemption City, KM 46, Lagos/Ibadan Expressway, Ibafo
                                </li>

                                <li>
                                    Glendora Bookshop <br />
                                    Ikeja City Mall Alausa, Ikeja, Lagos
                                </li>

                                <li>
                                    Glendora Bookshop<br />
                                    Shop C4, Falomo Shopping Centre Ikoyi, Lagos

                                </li>


                                <li>
                                    Jazzhole<br />
                                    168 Awolowo Road Ikoyi, Lagos
                                </li>

                                <li>
                                    Glendora Bookshop <br />
                                    Reception Lounge <br />
                                    Eko Le Meridien Hotel <br />
                                    Victoria Island, Lagos
                                </li>

                                <li>
                                    Glendora Bookshop <br />
                                    Departure Hall<br />
                                    MMA2 New Local Airport Ikeja, Lagos
                                </li>

                                <li>
                                    Glendora Bookshop<br />
                                    Departure Hall<br />
                                    Murtala Mohammed International Airport Ikeja, Lagos
                                </li>

                                <li>
                                    Quintessence<br />
                                    Falomo Shopping Centre <br />
                                    Ikoyi, Lagos
                                </li>


                        </ul>

                </div>




                <div class="inner-footer">

                    <div class="ankara-02" id="footer-ankara-disc">
                        &nbsp;
                    </div>


                    <div class="inner-footer-wrap">

                        <?php get_template_part( 'footer-quote' ); ?>

                        <?php get_template_part( 'social-links' ); ?>



                    </div>


                </div>
            </div>

        </div>

        <?php // get_sidebar(); ?>
        <?php get_footer(); ?>
