<?php
/*
Template Name: Custom Page Template - Default
*/
?>


        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/home.css" />
        <style type="text/css" media="screen">
            .inner-content-wrap p {
                width: 80%;
                margin: 0 auto;
                margin-bottom: 20px;

                font-size: 90%;
            }


        </style>

        <?php get_header(); ?>


        <div class="content">
            <div class="wrap" id="main-content-wrap" >


                <div class="inner-content-wrap clearfix">


                    <?php get_template_part( 'page-title' ); ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>


			<?php endwhile; // end of the loop. ?>

                </div>





                <div class="inner-footer">

                    <div class="ankara-02" id="footer-ankara-disc">
                        &nbsp;
                    </div>


                    <div class="inner-footer-wrap">

                        <?php get_template_part( 'footer-quote' ); ?>

                        <?php get_template_part( 'social-links' ); ?>



                    </div>


                </div>
            </div>

        </div>

        <?php // get_sidebar(); ?>
        <?php get_footer(); ?>

        
