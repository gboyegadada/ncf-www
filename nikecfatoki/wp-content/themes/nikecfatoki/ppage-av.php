<?php
/*
Template Name: Custom Page Template - Video and Audio
*/
?>


<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/av.css" />
        <style type="text/css" media="screen">
            .inner-content-wrap {
                width: 85%;
                margin: 0 auto;
                margin-bottom: 20px;

                font-size: 90%;
            }


        </style>

        <?php get_header(); ?>


        <div class="content">
            <div class="wrap" id="main-content-wrap" >


                <div class="inner-content-wrap clearfix">

                    <?php

                    $idObj = get_category_by_slug('videoaudio');

                    $args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 1);
                    $myposts = get_posts( $args );

                    if ( !empty($myposts) ) : ?>

                            <?php /* The loop */ ?>
                            <?php foreach( $myposts as $post ) :  ?>

                                    <?php

                                    setup_postdata($post);

                                    get_template_part( 'content', 'av' );

                                    ?>

                            <?php endforeach; ?>


                    <?php else : ?>
                            <?php get_template_part( 'content', 'none' ); ?>
                    <?php endif; ?>




                    <?php get_paging_nav(); ?>
                </div>









                <div class="inner-footer">

                    <div class="ankara-02" id="footer-ankara-disc">
                        &nbsp;
                    </div>


                    <div class="inner-footer-wrap">

                        <p class="footer-quote">
                            Your life is a book. Some have written half-way, some, three-quarter way.
                            You have the opportunity to write and rewrite the book of your life. Make
                            sure you write a masterpiece.
                        </p>

                        <?php get_template_part( 'social-links' ); ?>


                    </div>


                </div>
            </div>

        </div>

        <?php // get_sidebar(); ?>
        <?php get_footer(); ?>

        
