<?php

header('Content-Type: application/javascript');


$_regexp = '/^[-\.A-Za-z0-9_]{1,150}$/';

if (isset($_GET['q']) && is_string($_GET['q'])) {
    $_ids = explode(' ', $_GET['q']);
    foreach ($_ids as $k=>$v) $_ids[$k] = filter_var($v, FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));
}
$_base = filter_var($_GET['b'], FILTER_VALIDATE_REGEXP, array('options'=>array('regexp' => $_regexp)));


if (!isset($_ids) || $_ids === FALSE) { die('throw new TypeError ("Invalid module IDs!");'); }
if ($_base === FALSE) { die('throw new TypeError ("Invalid module base!");'); }



$_file = '';
$_failed = $_loaded = $_paths = array ();

foreach ($_ids as $_id) {
    
    $_path_to_file = './wp/wp-content/themes/optg/library/js/libs/' . $_base . '/' . $_id . '.js';

    if (file_exists($_path_to_file)) { 
        $_file .= file_get_contents ($_path_to_file); 
        $_loaded[] = "'" . $_id . "'";
    }
    else { 
        // die('Module (' . $_id . ') not found.'); 
        $_failed[] = "'" . $_id . "'";
        
    }
    
}



echo ''
. "/* --------------- status  --------------- */\n\n"
. ';(function (m) { '
. ' var report = { loaded: [' . implode(', ', $_loaded) . '], failed: [' . implode(', ', $_failed) . '] };'
. '/* Filter out failed modules... */'
. 'var k, i = report.failed.length;'
. 'if (i < 1) return;'

. 'while (i--) {'
. '    k = m._private.modules.indexOf(report.failed[i]);'
. '    if (k > -1) m._private.modules = m._private.modules.splice(k, 1); '
. '}'
. ' return m;'
 
. '}(Utils));'
. "\n\n"
. '';




echo $_file;


echo ''
. "/* --------------- seal  --------------- */\n\n"
. ';(function (m) {'
. ' m._private._doReady();'
        
. ' return m;'
 
. '}(Utils));'
. "\n\n"
. '';




exit(0);