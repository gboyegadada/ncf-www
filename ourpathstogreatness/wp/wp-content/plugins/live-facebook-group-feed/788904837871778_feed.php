<?php

$feed = array (
  '788904837871778_934143990014528' => 
  array (
    'id' => '788904837871778_934143990014528',
    'link' => 'http://woman.ng/2016/04/how-i-started-my-business-kasope-ladipo-ajai-founder-of-omo-alata-foods/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCSmWkvHRHbrDPm&w=130&h=130&url=http%3A%2F%2Fwoman.ng%2Fwp-content%2Fuploads%2F2016%2F04%2Fkasope.jpg&cfs=1&sx=76&sy=0&sw=424&sh=424',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBlFVhETUmYk2la&url=http%3A%2F%2Fwoman.ng%2Fwp-content%2Fuploads%2F2016%2F04%2Fkasope.jpg',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-04-05 19:15:46',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_934022246693369' => 
  array (
    'id' => '788904837871778_934022246693369',
    'message' => 'Gbenga Lasisi is the founder of Afro Tech Talent; a social media initiative that began in 2015 with an overall focus of providing a platform to showcase tech startups and technology talents emerging from sub-Saharan Africa. The platform aims to connect these startups and tech talents to potential investors. He is a published poet and story teller. 

Professionally, Gbenga has a background in Electrical Engineering and telecommunications. He moved to the USA in pursuit of career advancement 8 years ago and obtained a Master’s of Science degree at George Washington University.  He currently works as a management consultant at PricewaterhouseCoopers. He advises C-level executives across industries on IT infrastructure transformation initiatives. He has also worked with the World Bank as a technology consultant.

Gbenga believes that African stories are better told by Africans. He is passionate about telling these stories and uses the medium of poetry, theater arts, public speaking and social media to highlight the continent’s potential and wealth of talents.  Gbenga is married and lives in Maryland with his wife. Gbenga will be performing spoken word at the “Wear Your Africa” Fundraising gala on Sunday, May 22, 2016. Get your tickets at https://www.eventbrite.com/e/our-paths-to-greatness-inc-wear-your-africa-fundraising-gala-tickets-23744762223',
    'link' => 'https://www.facebook.com/optgAfrica/photos/a.789512777810984.1073741828.788904837871778/934022246693369/?type=3',
    'picture' => 'https://fbcdn-photos-e-a.akamaihd.net/hphotos-ak-xfa1/v/t1.0-0/s130x130/12920370_934022246693369_7348115145467748771_n.jpg?oh=b675150b89e8c48c487331ea1a21f9b9&oe=57C05575&__gda__=1467151527_2f132ad9ea6629028ac44a7c02c258c0',
    'full_picture' => 'https://scontent.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/q81/s720x720/12920370_934022246693369_7348115145467748771_n.jpg?oh=eb5fdfef29a2072f24206cc5f979b460&oe=578CBB21',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-04-05 14:42:35',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_933570000071927' => 
  array (
    'id' => '788904837871778_933570000071927',
    'message' => 'May 21st and 22nd "Eyes on Africa" event flyer updated. Have you registered for the free events and bought your ticket for the "Wear Your Africa" Gala?
https://www.eventbrite.com/e/our-paths-to-greatness-inc-wear-your-africa-fundraising-gala-tickets-23744762223',
    'link' => 'https://www.facebook.com/optgAfrica/photos/a.788919221203673.1073741827.788904837871778/933569876738606/?type=3',
    'picture' => 'https://fbcdn-photos-f-a.akamaihd.net/hphotos-ak-xal1/v/t1.0-0/s130x130/12670358_933569876738606_3287143872711348921_n.jpg?oh=a1272d1eb3e5f1f7a28dc82f2e27cc62&oe=57B81903&__gda__=1467680368_f48434d594793dbb0295aa9b395c0151',
    'full_picture' => 'https://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-xal1/v/t1.0-9/s720x720/12670358_933569876738606_3287143872711348921_n.jpg?oh=d4aadb02dbc3854467d704968a9212da&oe=577255FC&__gda__=1471614095_30b564404a57f48ed1cac8a34ac4cad5',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-04-04 18:36:20',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_933559103406350' => 
  array (
    'id' => '788904837871778_933559103406350',
    'message' => 'We\'re bursting with excitement to tell you who the keynote speaker is for the gala on May 22nd. Can you guess? #optglaunch2016',
    'type' => 'status',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-04-04 18:01:33',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_933451546750439' => 
  array (
    'id' => '788904837871778_933451546750439',
    'message' => 'Born in Lagos, Nigeria, Roye Okupe is a veteran creative specialist who holds both a Bachelor’s and Master’s in computer science from The George Washington University. His passion for animation led him to found YouNeek Studios in 2012, an avenue that would allow him pursue his dream of creating a diverse library of superheroes. Under that umbrella, Roye wrote and produced several animated productions including, but not limited to, 2D/3D animated short films, TV commercials, show openers, music videos and much more. Roye is no stranger to wearing multiple hats. He was part of a team (as a producer, assistant director and editor) of filmmakers who retold the Biblical story of King David in a contemporary movie titled D’Comeback, which premiered in the US in March 2010. With the superhero genre currently at the height of popularity, Roye has made it a goal to create a connected universe of heroes, with origins from locations that are currently neglected and/or ignored. In April 2015, Roye released chapter 1 of his debut, superhero graphic novel titled: E.X.O. The Legend of Wale Williams, a superhero story set in a futuristic Nigeria. E.X.O. was received with critical acclaim and has since been featured on CNN, Forbes, BBC, The Huffington Post, Mashabe and more!

Roye will be speaking on May 21st at the panel discussion African Cartoons and Comics: Educating the Present and Future Generation with Adamu Waziri, creator of Bino and Fino African cartoons, moderated by Jola Naibi, author of Terra Cotta Beauty.  Register at:  https://www.eventbrite.com/e/our-paths-to-greatness-inc-presents-african-cartoons-and-comics-educating-the-present-and-future-tickets-22227075782',
    'link' => 'https://www.facebook.com/optgAfrica/photos/a.789512777810984.1073741828.788904837871778/933451546750439/?type=3',
    'picture' => 'https://fbcdn-photos-a-a.akamaihd.net/hphotos-ak-xaf1/v/t1.0-0/s130x130/12936683_933451546750439_6701465134369551701_n.jpg?oh=939e85c6d8b48276fc80e80f7feee7d8&oe=578697DB&__gda__=1468625980_9f09c569240977f4989bafe2f5a0ca1a',
    'full_picture' => 'https://scontent.xx.fbcdn.net/hphotos-xaf1/v/t1.0-9/q81/s720x720/12936683_933451546750439_6701465134369551701_n.jpg?oh=e5a6c3941e5dce6fa6c73d1e9c07c8a4&oe=5784B18F',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-04-04 14:21:44',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_932945060134421' => 
  array (
    'id' => '788904837871778_932945060134421',
    'message' => 'Etim Eyo is the founder of Nigerian Nostalgia Project  (NNP) a social media Initiative that began in 2010 with an overall focus on shaping the collective identity of Nigeria, through the examination of collective thought, standards and values and the pursuit of common ideals for Nigerians. 

Etim Eyo’s educational background is in the areas of Law, Business, and Technology Management. Professionally, he is a management consultant with specialization in financial operations and technology risk. He has supported the financial management and technology undertakings of several major US public administration agencies as well as major commercial organizations. Mr Eyo gained his career experience at global accounting firm KPMG LLP, systems integration giant, Accenture LLP and later independently, through his own firm, Afriwest Consulting LLC.

He does not see history as being so far removed from his financial accountability background, but sees rather that the robust documentation and accessibility of what would become history, as a primary channel through which to infuse accountability into systems and environments where it is lacking. An alumni of King’s College Lagos, he lives part of the time in Lagos and Washington DC with his family.

Etim will be speaking at the panel discussion on May 21st - Eyes on Africa: Learning from Our Past, Navigating the Present, Grasping the Future."  Register at https://www.eventbrite.com/e/our-paths-to-greatness-inc-presents-panel-discussion-on-eyes-on-africa-learning-from-our-past-tickets-22228978473',
    'link' => 'https://www.facebook.com/optgAfrica/photos/a.789512777810984.1073741828.788904837871778/932945060134421/?type=3',
    'picture' => 'https://fbcdn-photos-a-a.akamaihd.net/hphotos-ak-xlf1/v/t1.0-0/p130x130/11181200_932945060134421_1361698086797436523_n.jpg?oh=a81c8bc068cbb12217ff3c1724e3361d&oe=5774DA87&__gda__=1467553280_e05d095a09e559cd48aa5db97a8e275c',
    'full_picture' => 'https://scontent.xx.fbcdn.net/hphotos-xlf1/v/t1.0-9/p720x720/11181200_932945060134421_1361698086797436523_n.jpg?oh=130b767835112be3e2d7de56ded3c255&oe=5778FE78',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-04-03 16:55:59',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_932598426835751' => 
  array (
    'id' => '788904837871778_932598426835751',
    'link' => 'http://edition.cnn.com/videos/world/2016/04/01/african-voices-music-spc-b.cnn',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDXVbI07OWQLzoG&w=130&h=130&url=https%3A%2F%2Fwww.facebook.com%2Fads%2Fimage%2F%3Fd%3DAQJjdyCzHoVOK2VAUt2J9Gc4NqNr4oZpTDyjBCvmN8B_roPhMeecYPV_FLSkmUgfLxUIiiTcTVJRTWSWabV1ypDVWTcFmg4aRmKTnCmg9C9q7ZpLUHjo6Hyx4iEhFN8zpxjyp02fJe7SkvstqWko1OIc&cfs=1&sx=47&sy=0&sw=259&sh=259',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQAbt42IYVe__el6&url=https%3A%2F%2Fwww.facebook.com%2Fads%2Fimage%2F%3Fd%3DAQJjdyCzHoVOK2VAUt2J9Gc4NqNr4oZpTDyjBCvmN8B_roPhMeecYPV_FLSkmUgfLxUIiiTcTVJRTWSWabV1ypDVWTcFmg4aRmKTnCmg9C9q7ZpLUHjo6Hyx4iEhFN8zpxjyp02fJe7SkvstqWko1OIc',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-04-03 01:11:52',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_930848260344101' => 
  array (
    'id' => '788904837871778_930848260344101',
    'message' => 'Jason Nkwain is an Artist, Writer, Poet, and Teacher who was born in Cameroon, and moved to the USA at the age of thirteen. He has been speaking publicly since the age of seven when he started reciting poems and rhymes for kindergarten events. Jason became really interested in Poetry after moving to the US, and as time went by, he slowly developed a love for performance poetry or as most people call it, Spoken Word poetry. In 2012, alongside some of his close Cameroonian friends, he co-founded LEGACY ENTERTAINMENT, which is a collective of African artists whose main focus are to elevate and expose the beauty and the brilliance of the African Art. Jason Nkwain’s poems focus on the continent of Africa, its people and a critical analysis of self. Looking at the African people through the eyes of an Anthropologist, Jason seeks to expose the beauty in Africa’s story. Jason seeks to dispel most myths and to shatter the false His-Stories, creating room for the truth in Our-Stories. Some of his well-known poems are Thoughts, and Have You Ever Seen An African Dance. Jason Nkwain is a recent graduate of The University of Maryland College Park with a double Degree in Geographical Information Systems and English. He is currently a high school English teacher and hopes to be an English professor someday focusing on African studies. 

Jason will be performing spoken word at the launch.  Have you bought your ticket(s)?  https://www.eventbrite.com/e/our-paths-to-greatness-inc-wear-your-africa-fundraising-gala-tickets-23744762223

https://www.youtube.com/watch?v=2LoL9WQZI5A',
    'link' => 'https://www.youtube.com/watch?v=2LoL9WQZI5A',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBgNQX9D6OC6UN5&w=130&h=130&url=https%3A%2F%2Fi.ytimg.com%2Fvi%2F2LoL9WQZI5A%2Fmaxresdefault.jpg&cfs=1&sx=406&sy=0&sw=720&sh=720',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDfnx6uNFEoSn74&w=720&h=720&url=https%3A%2F%2Fi.ytimg.com%2Fvi%2F2LoL9WQZI5A%2Fmaxresdefault.jpg&cfs=1&sx=406&sy=0&sw=720&sh=720',
    'type' => 'video',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-03-31 15:47:22',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_930713980357529' => 
  array (
    'id' => '788904837871778_930713980357529',
    'message' => 'Unveiling our official logo. You like?',
    'link' => 'https://www.facebook.com/optgAfrica/photos/a.788918981203697.1073741826.788904837871778/930713950357532/?type=3',
    'picture' => 'https://fbcdn-photos-b-a.akamaihd.net/hphotos-ak-xpf1/v/t1.0-0/s130x130/12321443_930713950357532_7281429169638364011_n.jpg?oh=bc4a57baeb9fe6f74825ac01df901185&oe=57BB66B9&__gda__=1471910660_92a65885b3393ead302ce6b5cc212ffb',
    'full_picture' => 'https://scontent.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/s720x720/12321443_930713950357532_7281429169638364011_n.jpg?oh=4cd07852a4ce05d023c098f2b498c972&oe=578D9C46',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-03-31 12:37:04',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_930030190425908' => 
  array (
    'id' => '788904837871778_930030190425908',
    'message' => 'Malebo Gololo was featured on Path to Greatness Series last year. (http://nikecfatoki.com/blog/2015/07/malebo-gololo-making-her-mark-in-the-world.html)
She\'s speaks with OPTG from South Africa on how she is walking her path to greatness https://www.youtube.com/watch?v=8CWyarUspGA&feature=youtu.be',
    'link' => 'https://www.youtube.com/watch?v=8CWyarUspGA&feature=youtu.be',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBkhs9xldXsY32Y&w=130&h=130&url=https%3A%2F%2Fi.ytimg.com%2Fvi%2F8CWyarUspGA%2Fhqdefault.jpg&cfs=1&sx=46&sy=0&sw=360&sh=360',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCTpu9W0-IzuxPG&w=360&h=360&url=https%3A%2F%2Fi.ytimg.com%2Fvi%2F8CWyarUspGA%2Fhqdefault.jpg&cfs=1&sx=46&sy=0&sw=360&sh=360',
    'type' => 'video',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-03-30 17:42:17',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_914366885325572' => 
  array (
    'id' => '788904837871778_914366885325572',
    'message' => 'The Lijadu Sisters give words of wisdom.  The way they end each others sentences - priceless!',
    'link' => 'https://www.facebook.com/okayafrica/videos/1148599171825246/',
    'picture' => 'https://fbcdn-vthumb-a.akamaihd.net/hvthumb-ak-xpt1/v/t15.0-10/s130x130/12672503_1148600668491763_183327203_n.jpg?oh=7da53063a9fc6c1701add3ebd2ad9837&oe=57579E8F&__gda__=1468975556_e96974f90bf1c49f3fd092e013662449',
    'full_picture' => 'https://fbcdn-vthumb-a.akamaihd.net/hvthumb-ak-xpt1/v/t15.0-10/s720x720/12672503_1148600668491763_183327203_n.jpg?oh=ace03134f78c016766d2d6bf840cb086&oe=578D98F3&__gda__=1468769720_d7ceef06db00cb4b01a39ad7f410ac90',
    'type' => 'video',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-03-11 18:48:33',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_914322725329988' => 
  array (
    'id' => '788904837871778_914322725329988',
    'message' => 'When you hear \'Somalia,\'what comes to mind? The media tends to distort views, which is why we must start to tell our own stories. See Somalia once more through the eyes of a Somalian.',
    'link' => 'http://www.okayafrica.com/news/everyday-horn-of-africa-photos-mustafa-saeed/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCrsTKGd6XPoGfZ&w=130&h=130&url=http%3A%2F%2Fwww.okayafrica.com%2Fwp-content%2Fuploads%2FEveryday-Horn-of-Africa-Girma-Berta-.jpg&cfs=1',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQA3WfWWlNLGCN22&url=http%3A%2F%2Fwww.okayafrica.com%2Fwp-content%2Fuploads%2FEveryday-Horn-of-Africa-Girma-Berta-.jpg',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-03-11 17:06:22',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_913430722085855' => 
  array (
    'id' => '788904837871778_913430722085855',
    'link' => 'http://www.wearethecity.com/meet-first-female-publisher-british-gq-first-black-publisher-conde-nast-uk-leading-charge/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQD5UwU9VL4A-p_6&w=130&h=130&url=http%3A%2F%2Fwww.wearethecity.com%2Fwp-content%2Fuploads%2F2016%2F03%2FVanessa-Kingori-British-GQ-featured.jpeg&cfs=1&sx=52&sy=0&sw=300&sh=300',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCXD69XdQySIToq&url=http%3A%2F%2Fwww.wearethecity.com%2Fwp-content%2Fuploads%2F2016%2F03%2FVanessa-Kingori-British-GQ-featured.jpeg',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-03-09 23:00:24',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_913316655430595' => 
  array (
    'id' => '788904837871778_913316655430595',
    'message' => 'Ikhide Ikheloa is a literary critic and social commentator.  Over the years, he has  established himself as an authoritative voice on African literature, social issues and politics. His strong views have gotten the attention of many on social media channels which he uses successfully to attract a following all over the African continent.  These views and commentaries often laced with humor, wit, and sarcasm are both informative and thought-provoking, and keep his followers and critics coming back for more.  

Pa Ikhide, works in the education sector of Montgomery County Government, Maryland and has lived in the United States since 1982.  We are pleased to welcome Ikhide Ikheloa to the panel discussion on “Eyes on Africa: Learning from Our Past, Navigating the Present, Grasping the Future.” Save the date, May 21, 2016.  Registration is now open!  https://www.eventbrite.com/e/our-paths-to-greatness-inc-presents-panel-discussion-on-eyes-on-africa-learning-from-our-past-tickets-22228978473',
    'link' => 'https://www.facebook.com/788904837871778/photos/a.789512777810984.1073741828.788904837871778/913316655430595/?type=3',
    'picture' => 'https://fbcdn-photos-h-a.akamaihd.net/hphotos-ak-xfp1/v/t1.0-0/s130x130/247178_913316655430595_5419512698662117311_n.jpg?oh=ef2e6458c4ea0387e4dbdaa5f850739c&oe=575BF131&__gda__=1465721508_789ef831486e5d0a891ed58322efd249',
    'full_picture' => 'https://scontent.xx.fbcdn.net/hphotos-xfp1/v/t1.0-9/s720x720/247178_913316655430595_5419512698662117311_n.jpg?oh=619a8f195da49f9e767c9ef1ba726bee&oe=578C0DD4',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-03-09 18:09:31',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_864516080310653' => 
  array (
    'id' => '788904837871778_864516080310653',
    'message' => 'The first Bino and Fino dolls are here. Congrats to the creator, Adamu Waziri. Nothing beats seeing a vision birthed.',
    'link' => 'https://www.facebook.com/binoandfino/videos/1089024944463335/',
    'picture' => 'https://fbcdn-vthumb-a.akamaihd.net/hvthumb-ak-xpf1/v/t15.0-10/s130x130/12323414_1089056931126803_272384928_n.jpg?oh=b6a54e568cb6c593c2654f6f0bb56f70&oe=56F29F4A&__gda__=1457443703_65f2bd7eff37c59a3e1a0ec3c923297c',
    'full_picture' => 'https://fbcdn-vthumb-a.akamaihd.net/hvthumb-ak-xpf1/v/t15.0-10/s720x720/12323414_1089056931126803_272384928_n.jpg?oh=3cfa07e251983c72dca6c371a4923786&oe=56E1FC36&__gda__=1458935819_a2aab1298d11344c8b75e15aa40094b6',
    'type' => 'video',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-12-01 19:19:03',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_864447856984142' => 
  array (
    'id' => '788904837871778_864447856984142',
    'message' => 'Celebrating the life of Dr. Oyebade Dosumnu.  His life speaks volumes of one that was lived to the fullest.  He was the "artistic director and conductor of the Fela Sowande Singers. He held a B.A. in music (vocal performance) from Obafemi Awolowo University, Ile-Ife, and Masters and Doctoral degrees in ethnomusicology from the University of Pittsburgh, where he also earned a graduate certificate in African studies." 

Here is celebrating Dr. Dosunmu. He lived a full life and left a legacy.  https://felasowandesingers.wordpress.com/oyebade-dosunmu/

Listen to "Emi o Gbe Oju Mi Soke Woni"
https://www.youtube.com/watch?v=eSQZSxJOAhQ',
    'link' => 'https://felasowandesingers.wordpress.com/oyebade-dosunmu/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQAyFeDBQxnsuZD-&w=130&h=130&url=https%3A%2F%2Ffelasowandesingers.files.wordpress.com%2F2013%2F08%2Fface-shot.jpg%3Fw%3D269&cfs=1&sx=0&sy=0&sw=269&sh=269',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCQfuSh_wND4MgA&url=https%3A%2F%2Ffelasowandesingers.files.wordpress.com%2F2013%2F08%2Fface-shot.jpg%3Fw%3D269',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-12-01 15:44:15',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_864409553654639' => 
  array (
    'id' => '788904837871778_864409553654639',
    'message' => 'http://allafrica.com/stories/201511271623.html',
    'link' => 'http://allafrica.com/stories/201511271623.html',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDlwlVT5_CnOCrl&w=130&h=130&url=http%3A%2F%2Fallafrica.com%2Fdownload%2Fpic%2Fmain%2Fmain%2Fcsiid%2F00301862%3Afa38da9ce721177fcad4b91c6d78a68c%3Aarc614x376%3Aw1200.png&cfs=1&sx=294&sy=0&sw=627&sh=627',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCNdw9KocXeMbdJ&url=http%3A%2F%2Fallafrica.com%2Fdownload%2Fpic%2Fmain%2Fmain%2Fcsiid%2F00301862%3Afa38da9ce721177fcad4b91c6d78a68c%3Aarc614x376%3Aw1200.png',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-12-01 13:46:19',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_864214920340769' => 
  array (
    'id' => '788904837871778_864214920340769',
    'message' => 'https://www.washingtonpost.com/news/wonk/wp/2015/11/30/meet-the-millennial-who-coaches-people-who-feel-trapped-by-bureaucracy/',
    'link' => 'https://www.washingtonpost.com/news/wonk/wp/2015/11/30/meet-the-millennial-who-coaches-people-who-feel-trapped-by-bureaucracy/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQAVJcOjq3TmtOaL&w=130&h=130&url=http%3A%2F%2Fwww.washingtonpost.com%2Fblogs%2Fwonkblog%2Ffiles%2F2015%2F11%2FDSCF2730.jpg&cfs=1&sx=1115&sy=0&sw=3264&sh=3264',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDbewTRPXesMOUg&url=http%3A%2F%2Fwww.washingtonpost.com%2Fblogs%2Fwonkblog%2Ffiles%2F2015%2F11%2FDSCF2730.jpg',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-30 23:25:57',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_864075097021418' => 
  array (
    'id' => '788904837871778_864075097021418',
    'link' => 'https://www.facebook.com/DesmondTutuOfficial/photos/a.726745924136076.1073741837.354381764705829/726745950802740/?type=3',
    'picture' => 'https://fbcdn-photos-g-a.akamaihd.net/hphotos-ak-xft1/v/t1.0-0/s130x130/12316647_726745950802740_4539038956655373605_n.jpg?oh=39563b8cede0a992e1789dc3fb232523&oe=56EEA6EE&__gda__=1457021180_46cdbd6410a62302508b40b040deeaf1',
    'full_picture' => 'https://scontent.xx.fbcdn.net/hphotos-xlf1/t31.0-8/s720x720/12309795_726745950802740_4539038956655373605_o.jpg',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-30 15:45:19',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_863350297093898' => 
  array (
    'id' => '788904837871778_863350297093898',
    'message' => 'It took going away for her to appreciate her culture. So many lessons to learn from this.',
    'link' => 'https://www.facebook.com/humansofnewyork/photos/a.102107073196735.4429.102099916530784/1136761036397995/?type=3',
    'picture' => 'https://fbcdn-photos-h-a.akamaihd.net/hphotos-ak-xap1/v/t1.0-0/s130x130/12311046_1136761036397995_6373185179313036785_n.jpg?oh=3b02ea64ef15b25e7cd2cd77827f7196&oe=56AD3907&__gda__=1458562623_23dc161ee4f33e9d706265854c8c1ce6',
    'full_picture' => 'https://fbcdn-photos-h-a.akamaihd.net/hphotos-ak-xap1/v/t1.0-0/p480x480/12311046_1136761036397995_6373185179313036785_n.jpg?oh=f1d4a25819eed07172e810040a7f4673&oe=56EE7B77&__gda__=1457649999_4fa9717b2b03f9cf1289ddf37ecf30a3',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-28 21:41:58',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_862836710478590' => 
  array (
    'id' => '788904837871778_862836710478590',
    'message' => '"My life as an artist is what I was born with." - Nike Davies-Okundaye',
    'link' => 'http://www.risingafrica.org/uncategorized/the-increadible-story-of-a-nigerian-batik-and-textile-designer-with-no-formal-education-but-lectures-at-harvard-other-top-universities/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBzF2kDNcEvGLyS&w=130&h=130&url=http%3A%2F%2Fi1.wp.com%2Fwww.risingafrica.org%2Fwp-content%2Fuploads%2F2015%2F11%2FNike-Okundaye.jpg%3Fresize%3D980%252C552&cfs=1&sx=106&sy=0&sw=552&sh=552',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQAmMe3zR2xosXdE&url=http%3A%2F%2Fi1.wp.com%2Fwww.risingafrica.org%2Fwp-content%2Fuploads%2F2015%2F11%2FNike-Okundaye.jpg%3Fresize%3D980%252C552',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-27 15:06:00',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_862013473894247' => 
  array (
    'id' => '788904837871778_862013473894247',
    'message' => 'When one realizes their purpose in life, all intents and actions begin to merge and pulsate into one.  Thread of Gold Beads play, Our Paths to Greatness, all crying out for a greater Africa, the Africa that we dream and long for, the Africa that we can have.  It is Time to Tell Our Stories. 

Save the date for the official launch of Our Paths to Greatness- two days of celebration, upliftment and sharing.  Speakers, events and activities are lining up and we can\'t wait for you to be a part of it.',
    'link' => 'https://youtu.be/-oLLJ0GikXM',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQB_CrXFLjUz5qZG&w=130&h=130&url=https%3A%2F%2Fi.ytimg.com%2Fvi%2F-oLLJ0GikXM%2Fmaxresdefault.jpg&cfs=1&sx=436&sy=0&sw=720&sh=720',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCL_swDwVHRJ_yY&w=720&h=720&url=https%3A%2F%2Fi.ytimg.com%2Fvi%2F-oLLJ0GikXM%2Fmaxresdefault.jpg&cfs=1&sx=436&sy=0&sw=720&sh=720',
    'type' => 'video',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-25 16:04:27',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_861562847272643' => 
  array (
    'id' => '788904837871778_861562847272643',
    'message' => 'The Gambia outlaws practice of Female Genital Mutilation (FGM)',
    'link' => 'http://gu.com/p/4efbj/sfb',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQAOYs2s5pY9bBh-&w=130&h=130&url=https%3A%2F%2Fi.guim.co.uk%2Fimg%2Fmedia%2Fc7b94597e9f4c3b33e5d4b4c226aaa0bdf940af6%2F0_64_3980_2389%2Fmaster%2F3980.jpg%3Fw%3D1200%26q%3D85%26auto%3Dformat%26sharp%3D10%26s%3D57b6ecc4377db23c682af62b4db50ec5&cfs=1&sx=0&sy=0&sw=720&sh=720',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBGe9BGXzY-UOcI&url=https%3A%2F%2Fi.guim.co.uk%2Fimg%2Fmedia%2Fc7b94597e9f4c3b33e5d4b4c226aaa0bdf940af6%2F0_64_3980_2389%2Fmaster%2F3980.jpg%3Fw%3D1200%26q%3D85%26auto%3Dformat%26sharp%3D10%26s%3D57b6ecc4377db23c682af62b4db50ec5',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-24 14:40:57',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_861206273974967' => 
  array (
    'id' => '788904837871778_861206273974967',
    'message' => 'Film Africa may be over, but these seven films directed by African women are still available to watch this season.  http://www.msafropolitan.com/2015/10/7-films-directed-by-african-women-to-see-at-film-africa.html',
    'link' => 'http://www.msafropolitan.com/2015/10/7-films-directed-by-african-women-to-see-at-film-africa.html',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBAbaHizulc3gUu&w=130&h=130&url=http%3A%2F%2Fwww.msafropolitan.com%2Fwp-content%2Fuploads%2F2015%2F10%2FBetween-Rings-The-Esther-Phiri-Story.jpg&cfs=1&sx=633&sy=0&sw=2336&sh=2336',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDHMZm5fcY-G4kV&url=http%3A%2F%2Fwww.msafropolitan.com%2Fwp-content%2Fuploads%2F2015%2F10%2FBetween-Rings-The-Esther-Phiri-Story.jpg',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-23 15:21:40',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_860667647362163' => 
  array (
    'id' => '788904837871778_860667647362163',
    'link' => 'http://www.careerpoint-solutions.com/110-positions-massive-recruitment-un-jobs-in-kenya-nov-2015/?utm_campaign=shareaholic&utm_medium=facebook&utm_source=socialnetwork',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBriGay5LI6m_TK&w=130&h=130&url=http%3A%2F%2Fwww.careerpoint-solutions.com%2Fwp-content%2Fuploads%2F2015%2F06%2FUnited_Nations-1024x682.png&cfs=1',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCsoQ0-UrmPenQ_&url=http%3A%2F%2Fwww.careerpoint-solutions.com%2Fwp-content%2Fuploads%2F2015%2F06%2FUnited_Nations-1024x682.png',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-22 03:21:42',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_859793027449625' => 
  array (
    'id' => '788904837871778_859793027449625',
    'message' => 'The true life story of the damsel and outcast. Meet Olorunfunmi Adebajo and be inspired http://nikecfatoki.com/blog/2015/11/the-damsel-and-the-outcast.html',
    'link' => 'https://www.facebook.com/788904837871778/photos/a.811831162245812.1073741829.788904837871778/859792977449630/?type=3',
    'picture' => 'https://fbcdn-photos-b-a.akamaihd.net/hphotos-ak-xtf1/v/t1.0-0/s130x130/12241358_859792977449630_4451385944771584713_n.jpg?oh=e41b0edde945c0f45e587d239f574582&oe=56FA1ED2&__gda__=1458459900_374e2947c9d814b1b2d93572f6f630af',
    'full_picture' => 'https://fbcdn-photos-b-a.akamaihd.net/hphotos-ak-xtf1/v/t1.0-0/p480x480/12241358_859792977449630_4451385944771584713_n.jpg?oh=25c7e3aef56d2067262e655da3e2849e&oe=56DF14F4&__gda__=1458427354_5abd78618538650ee9565cc35bf3537b',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-19 20:00:45',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_859691374126457' => 
  array (
    'id' => '788904837871778_859691374126457',
    'message' => 'Meet Haben Girma, Harvard Law\'s first deaf - blind graduate http://thisisafrica.me/meet-haben-girma-harvard-laws-first-deaf-blind-graduate/',
    'link' => 'http://thisisafrica.me/meet-haben-girma-harvard-laws-first-deaf-blind-graduate/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBWcCJJRaiTdUi_&w=130&h=130&url=http%3A%2F%2Fthisisafrica.me%2Fwp-content%2Fuploads%2Fsites%2F4%2F2015%2F11%2Fobama-haben-posing.jpg&cfs=1&sx=188&sy=0&sw=420&sh=420',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQAhGdu_o-KyjcsA&url=http%3A%2F%2Fthisisafrica.me%2Fwp-content%2Fuploads%2Fsites%2F4%2F2015%2F11%2Fobama-haben-posing.jpg',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-19 14:10:58',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_859571947471733' => 
  array (
    'id' => '788904837871778_859571947471733',
    'link' => 'http://www.theguardian.com/world/2013/sep/08/cecile-kyenge-quest-for-tolerance',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQA5UwjcUZsJeb9x&w=130&h=130&url=https%3A%2F%2Fi.guim.co.uk%2Fimg%2Fstatic%2Fsys-images%2FObserver%2FPix%2Fpictures%2F2013%2F9%2F6%2F1378505072582%2FCecile-Kyenge-Italys-mini-011.jpg%3Fw%3D1200%26q%3D85%26auto%3Dformat%26sharp%3D10%26s%3Dc9285b848f71829e9c4801d5a6603bf9&cfs=1&sx=272&sy=0&sw=720&sh=720',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBoM5dalGnIH1Su&url=https%3A%2F%2Fi.guim.co.uk%2Fimg%2Fstatic%2Fsys-images%2FObserver%2FPix%2Fpictures%2F2013%2F9%2F6%2F1378505072582%2FCecile-Kyenge-Italys-mini-011.jpg%3Fw%3D1200%26q%3D85%26auto%3Dformat%26sharp%3D10%26s%3Dc9285b848f71829e9c4801d5a6603bf9',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-19 07:28:07',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_859030097525918' => 
  array (
    'id' => '788904837871778_859030097525918',
    'message' => 'http://www.aauw.org/what-we-do/educational-funding-and-awards/?fb_action_ids=10153753288701613&fb_action_types=og.likes',
    'link' => 'http://www.aauw.org/what-we-do/educational-funding-and-awards/?fb_action_ids=10153753288701613&fb_action_types=og.likes',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBj1pdOEkDNzUAS&w=130&h=130&url=http%3A%2F%2Fwww.aauw.org%2Ffiles%2F2013%2F01%2Finternational-studies.jpg&cfs=1&sx=20&sy=0&sw=170&sh=170',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQAg7aaeP_yBbfj6&url=http%3A%2F%2Fwww.aauw.org%2Ffiles%2F2013%2F01%2Finternational-studies.jpg',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-17 20:13:43',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_858924117536516' => 
  array (
    'id' => '788904837871778_858924117536516',
    'link' => 'http://www.viva-naija.com/nigerian-artist-wins-8th-yanghyun-prize-gets-18-million-naira-prize-money/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBqMr_LUjCp9VT0&w=130&h=130&url=http%3A%2F%2Fwww.viva-naija.com%2Fwp-content%2Fuploads%2F2015%2F11%2FScreenshot_2015-11-16-19-54-30-1.png&cfs=1&sx=0&sy=0&sw=545&sh=545',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDk_wLyNDX7IESb&url=http%3A%2F%2Fwww.viva-naija.com%2Fwp-content%2Fuploads%2F2015%2F11%2FScreenshot_2015-11-16-19-54-30-1.png',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-17 14:38:47',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_858644217564506' => 
  array (
    'id' => '788904837871778_858644217564506',
    'message' => 'The Bino and Fino worldwide release trailer.  We can\'t wait to share some exciting news of a collaboration. Coming soon!',
    'link' => 'https://www.facebook.com/binoandfino/videos/1072993922733104/',
    'picture' => 'https://fbcdn-vthumb-a.akamaihd.net/hvthumb-ak-xpt1/v/t15.0-10/s130x130/12106561_1073007356065094_1006476518_n.jpg?oh=02d45cdea8b8b5318234af5620991e28&oe=56B88084&__gda__=1455018780_95ffd9daf82091271f51de818ecaa9a8',
    'full_picture' => 'https://fbcdn-vthumb-a.akamaihd.net/hvthumb-ak-xpt1/v/t15.0-10/s720x720/12106561_1073007356065094_1006476518_n.jpg?oh=0ac65e5e6b000fcb498cb0972ab30f2b&oe=56FA6C54&__gda__=1454685388_cf65066f034b5edbbead0af04aebbc66',
    'type' => 'video',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-16 19:20:57',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_855886521173609' => 
  array (
    'id' => '788904837871778_855886521173609',
    'message' => 'Double Congrats to Nigeria!!',
    'link' => 'https://www.facebook.com/votebuhari/photos/a.10151830705027734.1073741825.51744382733/10153308446612734/?type=3',
    'picture' => 'https://fbcdn-photos-c-a.akamaihd.net/hphotos-ak-xfa1/v/t1.0-0/s130x130/11222235_10153308446612734_7995037238680190559_n.jpg?oh=cbad574e04e1c1e4faeb5c643fa3c4f3&oe=56EAF952&__gda__=1454755084_ba16a8cb922438df9d3687f801718b92',
    'full_picture' => 'https://scontent.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/s720x720/11222235_10153308446612734_7995037238680190559_n.jpg?oh=a104b2d054b55b928bab1b2d0977972f&oe=56ECA05F',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-09 11:15:13',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_854355164660078' => 
  array (
    'id' => '788904837871778_854355164660078',
    'link' => 'http://edition.cnn.com/2013/11/01/opinion/africas-secret-weapon-diaspora/index.html',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBlBbkbXrV7JkTa&w=130&h=130&url=http%3A%2F%2Fi2.cdn.turner.com%2Fcnnnext%2Fdam%2Fassets%2F131031103428-amini-kajunju-portrait-photo-t3-entertainment.jpg&cfs=1&sx=0&sy=5&sw=210&sh=210',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQAifM9rQ4rxPvAI&url=http%3A%2F%2Fi2.cdn.turner.com%2Fcnnnext%2Fdam%2Fassets%2F131031103428-amini-kajunju-portrait-photo-t3-entertainment.jpg',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-11-05 10:31:42',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_850945691667692' => 
  array (
    'id' => '788904837871778_850945691667692',
    'message' => 'African proverbs we can learn from via Afritorial http://afritorial.com/the-best-72-african-wise-proverbs/',
    'link' => 'http://afritorial.com/the-best-72-african-wise-proverbs/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQB9BYx3MXBgqely&w=130&h=130&url=https%3A%2F%2Fwww.facebook.com%2Fads%2Fimage%2F%3Fd%3DAQIufB4co_DMOhpfXa91NFo_1tKG0lEH7a1dyOL87naABJeiC0e9ETE6CjRX_f_55haWFUDJgE2RFgpZXSPCF78iw0-D_cD7i_IjGGi1Os0GQGIUvu_Cq7EKqn_gAi42Rar5C-F0EK8G-yGOMj_mM0hh&cfs=1',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQAO0uhw9h5797Ht&url=https%3A%2F%2Fwww.facebook.com%2Fads%2Fimage%2F%3Fd%3DAQIufB4co_DMOhpfXa91NFo_1tKG0lEH7a1dyOL87naABJeiC0e9ETE6CjRX_f_55haWFUDJgE2RFgpZXSPCF78iw0-D_cD7i_IjGGi1Os0GQGIUvu_Cq7EKqn_gAi42Rar5C-F0EK8G-yGOMj_mM0hh',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-26 19:50:49',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_849488205146774' => 
  array (
    'id' => '788904837871778_849488205146774',
    'link' => 'http://www.risingafrica.org/business/ghanaian-millionaire-quits-microsoft-to-build-university-that-educates-young-africans/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDBJZCz2i5s2qu3&w=130&h=130&url=http%3A%2F%2Fi0.wp.com%2Fwww.risingafrica.org%2Fwp-content%2Fuploads%2F2015%2F01%2FPicture21.png%3Fresize%3D929%252C665&cfs=1',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBtfc1FmDJulCiJ&url=http%3A%2F%2Fi0.wp.com%2Fwww.risingafrica.org%2Fwp-content%2Fuploads%2F2015%2F01%2FPicture21.png%3Fresize%3D929%252C665',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-22 20:06:38',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_849471791815082' => 
  array (
    'id' => '788904837871778_849471791815082',
    'message' => '"There is no light without darkness." Via @ NigerianHeroes',
    'link' => 'https://www.facebook.com/nigerianheroesblog/photos/a.1011375658907443.1073741828.955673674477642/1061384537239888/?type=3',
    'picture' => 'https://fbcdn-photos-a-a.akamaihd.net/hphotos-ak-xfa1/v/t1.0-0/s130x130/10696174_1061384537239888_6935152470687199475_n.jpg?oh=d3e727623176fa47bb4c940fe9434225&oe=56CE3941&__gda__=1454886858_05c4d2dc0f7c23050931adac379e8200',
    'full_picture' => 'https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-xfa1/v/t1.0-9/s720x720/10696174_1061384537239888_6935152470687199475_n.jpg?oh=aae989de793e32a06524711ae52be424&oe=56860E54&__gda__=1456599775_14625e34352a5331b715105ae9741324',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-22 19:02:32',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_849084465187148' => 
  array (
    'id' => '788904837871778_849084465187148',
    'message' => '"On Monday (Oct. 19), students at three of South Africa’s top universities “shut down” their campuses with protests against fee increases for the 2016 academic year." 

The mass demonstrations began at the University of Witwatersrand in Johannesburg last week Wednesday (Oct. 14), after the university—ranked as the 2nd best South African university in the 2015 QS World University Rankings—announced that it would hike tuition fees by more than 10% next year, and would start demanding an upfront fee of 10,000 rand ($752) from each student at the beginning of the academic year.

Since then, protests have spread to the University of Cape Town and Rhodes University, with students at the picket lines arguing that the proposed hike would financially exclude mainly black and poor students."

@MaleboGololo  has been active and continues to update with her social media posts. We\'re praying for the safety of all and fair judgement by those in the halls of power.  Everyone is entitled to a good education. Everyone. 
http://qz.com/527572/south-african-students-are-protesting-fee-increases-by-shutting-down-universities/',
    'link' => 'http://qz.com/527572/south-african-students-are-protesting-fee-increases-by-shutting-down-universities/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDpc_DgCXoJAzvN&w=130&h=130&url=https%3A%2F%2Fqzprod.files.wordpress.com%2F2015%2F10%2Fstellies-cropped.jpg%3Fquality%3D80%26strip%3Dall%26w%3D1600&cfs=1&sx=231&sy=0&sw=900&sh=900',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBgfqrCfVPuUS2E&url=https%3A%2F%2Fqzprod.files.wordpress.com%2F2015%2F10%2Fstellies-cropped.jpg%3Fquality%3D80%26strip%3Dall%26w%3D1600',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-21 17:42:29',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_849037295191865' => 
  array (
    'id' => '788904837871778_849037295191865',
    'message' => 'Whatever happened to Aisha Nabukeera, the young lady running for Miss Uganda, scarred by her stepmother? http://matookerepublic.com/2015/07/11/aisha-nabukeera-crowned-miss-rising-woman-at-miss-uganda/',
    'link' => 'http://matookerepublic.com/2015/07/11/aisha-nabukeera-crowned-miss-rising-woman-at-miss-uganda/',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDW6eUYCCM1e7-b&w=130&h=130&url=http%3A%2F%2Fi0.wp.com%2Fmatookerepublic.com%2Fwp-content%2Fuploads%2F2015%2F07%2FNabukeera.jpg%3Fresize%3D620%252C412&cfs=1&sx=104&sy=0&sw=412&sh=412',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCrUbxeMOVYN2j8&url=http%3A%2F%2Fi0.wp.com%2Fmatookerepublic.com%2Fwp-content%2Fuploads%2F2015%2F07%2FNabukeera.jpg%3Fresize%3D620%252C412',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-21 14:55:27',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_848714278557500' => 
  array (
    'id' => '788904837871778_848714278557500',
    'message' => 'Josh Hartnett, Nicole Kidman perform monologues on written experiences of young South African children.
http://www.bbc.com/news/entertainment-arts-34540347',
    'link' => 'http://www.bbc.com/news/entertainment-arts-34540347',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDL3tP2xZ_sBpFn&w=130&h=130&url=http%3A%2F%2Fichef.bbci.co.uk%2Fnews%2F1024%2Fcpsprodpb%2FD900%2Fproduction%2F_86225555_86139833.jpg&cfs=1&sx=0&sy=0&sw=576&sh=576',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQAB72ieq40gdxrM&url=http%3A%2F%2Fichef.bbci.co.uk%2Fnews%2F1024%2Fcpsprodpb%2FD900%2Fproduction%2F_86225555_86139833.jpg',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-20 15:54:03',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_846875505408044' => 
  array (
    'id' => '788904837871778_846875505408044',
    'message' => 'Our Paths to Greatness: Unique Paths. Collective Greatness. Our slogan, yours too.',
    'link' => 'https://www.facebook.com/788904837871778/photos/a.788918981203697.1073741826.788904837871778/846875472074714/?type=3',
    'picture' => 'https://fbcdn-photos-d-a.akamaihd.net/hphotos-ak-xtp1/v/t1.0-0/s130x130/12112368_846875472074714_1351750749274431824_n.jpg?oh=6ef091c38229857f7579c0cfd718f33c&oe=56D12F64&__gda__=1455609075_57e48767fcc23b608923824f750f0f12',
    'full_picture' => 'https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-xtp1/v/t1.0-9/s720x720/12112368_846875472074714_1351750749274431824_n.jpg?oh=2a4364a3c2b378b2e27874eb3825388d&oe=56CA129B&__gda__=1455480844_2d2aba0eb699336b5d515facee3aa6eb',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-15 18:53:24',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_846784895417105' => 
  array (
    'id' => '788904837871778_846784895417105',
    'message' => 'Olufela Olusegun Oludotun Ransome-Kuti  was born on October 15, 1938, in Abeokuta, Nigeria, to a Protestant minister, Reverend Ransome-Kuti and Funmilayo Ransome-Kuit, a political activist. He was a first cousin to the Nigerian writer and Nobel laureate Wole Soyinka, the first African to win the Nobel Prize for Literature.

Growing up, Fela learned piano and drums, and led his school choir. He attended the Abeokuta Grammar School in Abeokuta.  In 1958, he moved to London to study medicine, but changed to Trinity College of Music to further his studies.  There he studied classical music and developed an awareness of American jazz.

Fela formed the Koola Lobitos band in 1963 which later became Afrika 70 and later Afrika 80.  In 1963, he moved back to Nigeria, re-formed Koola Lobitos and trained as a radio producer for the Nigerian Broadcasting Corporation. He played for some time with Victor Olaiya and his All Stars. He pioneered the Afrobeat, a combination of funk, jazz, salsa, Calypso and traditional Nigerian Yoruba music. 

Between the 1970 and 1980s, Fela began using his song lyrics to speak out against political tyranny and corruption.  This was heavily influenced by his 10 months spent in Los Angeles where he discovered the Black Power movement through Sandra Smith (now Sandra Izsadore), a partisan of the Black Panther Party.  Some of these songs include "Zombie," questioning Nigerian soldiers\' blind obedience to carrying out orders. Another, "V.I.P. (Vagabonds in Power)," seeks to empower the disenfranchised masses to rise up against the government; and the album Beasts of No Nation.  The Nigerian regime arrested Fela over 200 times and he was subjected to beatings. These experiences only inspired him to write more lyrics.  

Fela died on August 3, 1997 from complications of AIDS. Celebrating Fela Anikulapo-Kuti, who stood up for the common man and fought against tyrannical regimes even in the face of death.',
    'link' => 'https://www.facebook.com/788904837871778/photos/a.789512777810984.1073741828.788904837871778/846784655417129/?type=3',
    'picture' => 'https://fbcdn-photos-d-a.akamaihd.net/hphotos-ak-xpt1/v/t1.0-0/q82/s130x130/12088466_846784655417129_7721280509040643969_n.jpg?oh=7edac68f86a9f6a164f597efb35769aa&oe=568FB28B&__gda__=1456249983_2d62fe6f19ab0836342a1df970e134da',
    'full_picture' => 'https://scontent.xx.fbcdn.net/hphotos-xpt1/v/t1.0-9/12088466_846784655417129_7721280509040643969_n.jpg?oh=b9fa3db5fbddecb64cec6e0f737bcf03&oe=568B21E9',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-15 13:31:47',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_845301608898767' => 
  array (
    'id' => '788904837871778_845301608898767',
    'link' => 'http://www.myjoyonline.com/sports/2015/april-9th/didier-drogba-opens-first-of-five-hospitals-in-ivory-coast.php',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCyjPMcNlz2aHAK&w=130&h=130&url=http%3A%2F%2Fphotos.myjoyonline.com%2Fphotos%2Fnews%2F201109%2F150020206_26862.jpg&cfs=1&sx=5&sy=0&sw=220&sh=220',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQAdzzHfUh4TODNo&url=http%3A%2F%2Fphotos.myjoyonline.com%2Fphotos%2Fnews%2F201109%2F150020206_26862.jpg',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-11 17:48:00',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_844405792321682' => 
  array (
    'id' => '788904837871778_844405792321682',
    'message' => 'We\'re happy to report that Our Paths to Greatness is now a 501 (c) 3 Not-for-Profit organization. We\'re ready to make a positive impact and empower Africans worldwide!  Looking forward to working with extraordinary partners.  More to come.',
    'link' => 'https://www.facebook.com/788904837871778/photos/a.789512777810984.1073741828.788904837871778/844405792321682/?type=3',
    'picture' => 'https://fbcdn-photos-c-a.akamaihd.net/hphotos-ak-xta1/v/t1.0-0/s130x130/12141567_844405792321682_818403255194882487_n.jpg?oh=30a90a1d38999acce5ea4e5ce52be0cb&oe=56971B95&__gda__=1452644960_873f28dffff232225591dfb4575b5868',
    'full_picture' => 'https://scontent.xx.fbcdn.net/hphotos-xta1/v/t1.0-9/s720x720/12141567_844405792321682_818403255194882487_n.jpg?oh=1c72eeaa6b0bd984ffff5583773230e7&oe=56C37662',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-09 13:35:11',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_843730089055919' => 
  array (
    'id' => '788904837871778_843730089055919',
    'message' => 'Happy birthday to Desmond Tutu! Wishing you many more years.',
    'link' => 'https://www.facebook.com/788904837871778/photos/a.789512777810984.1073741828.788904837871778/843729895722605/?type=3',
    'picture' => 'https://fbcdn-photos-b-a.akamaihd.net/hphotos-ak-xpt1/v/t1.0-0/s130x130/12079655_843729895722605_2729909611668072163_n.jpg?oh=077d1d4998c8b5164d78486b56ddd1a0&oe=56C9890D&__gda__=1452502571_76a4120a85d1318b756235ecbbaafa31',
    'full_picture' => 'https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-xpt1/v/t1.0-9/12079655_843729895722605_2729909611668072163_n.jpg?oh=114061e94a4bd52a914c4074d6fd604f&oe=56D0B390&__gda__=1456261268_04c8068316ef60340d8b56f2a238867f',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-07 13:20:29',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_841543782607883' => 
  array (
    'id' => '788904837871778_841543782607883',
    'message' => 'Happy Independence day to Nigeria!',
    'link' => 'http://cnn.it/1GjTJlL',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCbQ4LyHNkTshcv&w=130&h=130&url=https%3A%2F%2Fwww.facebook.com%2Fads%2Fimage%2F%3Fd%3DAQJ0Fyl3eQFjUYk53Y-TRnTJM1nTQXg7oXzPY0BVQN-lUaf9PTGKmRTGE948G_w9T2MXSwtbUg8nOGfT3gl4Wp9ts_NDkUls8NMjHg2h93KXnElCMlEYvsSgq4AG_y0WjtPdskSKTtY5AD0XLigk_O7d&cfs=1&sx=221&sy=0&sw=404&sh=404',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQDyMkUaAbo__4vO&url=https%3A%2F%2Fwww.facebook.com%2Fads%2Fimage%2F%3Fd%3DAQJ0Fyl3eQFjUYk53Y-TRnTJM1nTQXg7oXzPY0BVQN-lUaf9PTGKmRTGE948G_w9T2MXSwtbUg8nOGfT3gl4Wp9ts_NDkUls8NMjHg2h93KXnElCMlEYvsSgq4AG_y0WjtPdskSKTtY5AD0XLigk_O7d',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-10-01 15:02:17',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_840955616000033' => 
  array (
    'id' => '788904837871778_840955616000033',
    'message' => '"I had travelled to Nigeria so many times and I never had any bad experience. That’s where I realized that there was a big gap between the perception people have about Nigeria from the news and the way Nigeria really is. So I decided it was high time to start a project that would change people’s view of Nigeria. And what better way to do that than to show examples of how hard working, successful and positive Nigerians are. That is how Nigerian Heroes came to life." - Janine Udogu featured on Paths to Greatness Series.

http://nikecfatoki.com/blog/2015/09/the-face-behind-nigerian-heroes.html',
    'link' => 'https://www.facebook.com/788904837871778/photos/a.789512777810984.1073741828.788904837871778/840955366000058/?type=3',
    'picture' => 'https://fbcdn-photos-b-a.akamaihd.net/hphotos-ak-xtp1/v/t1.0-0/s130x130/12079207_840955366000058_741498511501181153_n.jpg?oh=2f3db481a829a72eeeb767cf697d8dd1&oe=568D0333&__gda__=1456619226_bf399861fe55640699a32e6b60bb3ba1',
    'full_picture' => 'https://fbcdn-photos-b-a.akamaihd.net/hphotos-ak-xtp1/v/t1.0-0/p480x480/12079207_840955366000058_741498511501181153_n.jpg?oh=075dfe09ed85b04e3fe8a19dcfe77ecd&oe=5694BA4A&__gda__=1456607395_58e7a0bfba7e51d53200de532981997a',
    'type' => 'photo',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-09-29 20:00:26',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_840616139367314' => 
  array (
    'id' => '788904837871778_840616139367314',
    'message' => 'Muhtar Bakare:  Nigeria\'s revolutionary capitalist by Igoni Barrett',
    'link' => 'http://www.aljazeera.com/programmes/my-nigeria/2015/09/muhtar-bakare-nigeria-revolutionary-capitalist-150921095020723.html',
    'picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCAAejooRaLi6_a&w=130&h=130&url=http%3A%2F%2Fwww.aljazeera.com%2Fmritems%2FImages%2F2015%2F9%2F21%2F08bd52e723d643e0a81801ce52e2cbdb_18.jpg&cfs=1&sx=337&sy=0&sw=562&sh=562',
    'full_picture' => 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQBvK4RHKMyi5UIT&url=http%3A%2F%2Fwww.aljazeera.com%2Fmritems%2FImages%2F2015%2F9%2F21%2F08bd52e723d643e0a81801ce52e2cbdb_18.jpg',
    'type' => 'link',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2015-09-28 18:31:56',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
  '788904837871778_1673408009589612' => 
  array (
    'id' => '788904837871778_1673408009589612',
    'link' => 'https://www.facebook.com/events/1673408009589612/',
    'picture' => 'https://fbcdn-photos-g-a.akamaihd.net/hphotos-ak-xla1/v/t1.0-0/p130x130/1937083_913300232098904_3783359442330743279_n.jpg?oh=969ee12f4c0a5f57cb18a035396c267f&oe=574BEFF1&__gda__=1464650763_2f73e28e9fba9c8bbd7453246a25adef',
    'full_picture' => 'https://scontent.xx.fbcdn.net/hphotos-xla1/v/t1.0-9/p720x720/1937083_913300232098904_3783359442330743279_n.jpg?oh=e5a1a0451394fff0a04ec97d4d4cbf1d&oe=57611706',
    'type' => 'event',
    'created_time' => 
    DateTime::__set_state(array(
       'date' => '2016-03-09 17:41:12',
       'timezone_type' => 1,
       'timezone' => '+00:00',
    )),
  ),
);

?>