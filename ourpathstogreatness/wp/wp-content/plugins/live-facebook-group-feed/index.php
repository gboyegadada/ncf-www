<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*
Plugin Name: Live Facebook Group Feed
Plugin URI:  http://fapplabs.com
Description: Custom plugin for OPTG
Version:     1.0
Author:      Gboyega Dada
Author URI:  http://fapplabs.com
*/

require_once (dirname(__FILE__) . '/options.php');


add_shortcode('fbgroupfeed', 'fbgroupfeed');


function printfbgrpfeed($feed, $count)
{
	$countcheck = 0;
	$html ='';
	$html.= '<ul id="fbgrpfeed">';
    
    foreach($feed as $p) {
        if ($count > $countcheck) {
            $html.= '<li>';
            
            $html.= '<img src="' . urldecode($p['full_picture']) . '" />';
            $html.= '<p>' . $p['message'] . '</p>';
            $html.= '<p><a href="' . $p['link'] . '" target="_blank" />Read more</a></p>';
            
            
            $html.= '</li>';

            $countcheck = $countcheck + 1;
        }
    }

	$html.= '</ul>';
	return $html;
}







function fbgroupfeed($atts)
{
	$twiwihtml ='';
	extract(shortcode_atts(array(
		'group_id' => '00000000000',
		'count' => 5, 
        'print' => 1
	) , $atts));

	// Get the variables all parts need

	$options = get_option('fbgrpfeed_plugin_options');
	$file = plugin_dir_path(__FILE__) . $group_id . '_feed.php';

	if ($group_id == '' || $count == '' || $options['ck'] == '' || $options['cs'] == '') {
		if ($options['debug'] == 1){
			if($group_id==''){$debuginfo.= 'Your Facebook Group ID has not been set.';}
			if($count==''){$debuginfo.= 'The number of posts has not been set.';}
			if($options['ck']=='' | $options['cs']==''){$debuginfo.= 'Your API details are not complete.';}
					return 'Please ensure this plugin is correctly configured under "Facebook Group Options" <h3>Debug Details</h3>'.$debuginfo;


        }
	}
	else {
		if ($options['caching'] == 1 && file_exists($file) && time() - filemtime($file) < $options['cache_exp'] * 3600) {
			include (plugin_dir_path(__FILE__) . $group_id . '_feed.php');
		}
		else {
            require_once dirname(__FILE__) . '/lib/facebook-php-sdk-v4-5.0-dev/src/Facebook/autoload.php';
            
            

			if (!defined('FB_CONSUMER_KEY')) define('FB_CONSUMER_KEY', $options['ck']);
			if (!defined('FB_CONSUMER_SECRET')) define('FB_CONSUMER_SECRET', $options['cs']);


			if (file_exists($file) && $options['caching'] == 1) {
				include (plugin_dir_path(__FILE__) . $group_id . '_feed.php');
			}

			if (!isset($feed)) {
				$feed = array();
				$since_id = 0;
				$countlimit = $count;
				$searcharray = array(
					'group_id' => $group_id,
					'count' => $count
				);
			}
			else {
				$since_id = max(array_keys($feed));
				$searcharray = array(
					'group_id' => $group_id,
					'count' => 15,
					'since_id' => $since_id
				);
			}

            
            
            $fb = new Facebook\Facebook([
             'app_id' => FB_CONSUMER_KEY,
             'app_secret' => FB_CONSUMER_SECRET,
             'default_graph_version' => 'v2.5',
             ]);
            

            // GET FEED...
            $_fields = [
                'id', 'message', 'link', 'picture', 'full_picture', 'type', 'created_time'
            ];
            
            try {
              // Requires the "read_stream" permission
              $response = $fb->get('/'.$group_id.'/feed?fields=' . implode(',', $_fields) . '&limit='.$count.'&access_token='.FB_CONSUMER_KEY.'|'.FB_CONSUMER_SECRET);
            } catch(Facebook\Exceptions\FacebookResponseException $e) {
              // When Graph returns an error
              $debuginfo.= 'Graph returned an error: ' . $e->getMessage();
              return 'Please ensure this plugin is correctly configured under "Facebook Group Options" <h3>Debug Details</h3>'.$debuginfo;
              
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
              // When validation fails or other local issues
              $debuginfo.= 'Facebook SDK returned an error: ' . $e->getMessage();
              return 'Please ensure this plugin is correctly configured under "Facebook Group Options" <h3>Debug Details</h3>'.$debuginfo;
              
            }
            
            

            // Page 1
            $feedEdge = $response->getGraphEdge();


            foreach ($feedEdge as $status) {
                $data = $status->asArray();

                $feed[$data['id']] = $data;
                


                // $tweetcontent = preg_replace(array('|([\w\d]*)\s?(https?://([\d\w\.-]+\.[\w\.]{2,6})[^\s\]\[\<\>]*/?)|i','/\B\@([a-zA-Z0-9_]{1,20})/','/(^|\s)#(\w*[a-zA-Z_]+\w*)/'), array('$1 <a href="$2" target="_blank">$2</a>','<a href="https://twitter.com/#!/$1" target="_blank">$0</a>','\1<a href="http://twitter.com/search?q=%23\2" target="_blank">#\2</a>'), $tweetcontent);


                krsort($feed);
                
            }
            if ($options['caching'] == 1) {
                $var_str = var_export($feed, true);
                $var = "<?php\n\n\$feed = $var_str;\n\n?>";
                file_put_contents(plugin_dir_path(__FILE__) . $group_id . '_feed.php', $var);
            }
            
		}
        
            
        return ($print==1) ? printfbgrpfeed($feed, $count) : $feed;

        
	}
}







/*



require_once dirname(__FILE__) . '/facebook-php-sdk-v4/src/Facebook/autoload.php';
$fb = new Facebook\Facebook([
 'app_id' => '902787683144486',
 'app_secret' => 'ee001a5632fae97f2076f5f2c8a6153b',
 'default_graph_version' => 'v2.5',
 ]);

$_grp_id = '788904837871778';

try {
  // Requires the "read_stream" permission
  $response = $fb->get('/'.$_grp_id.'/feed?fields=id,message&limit=5');
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}


// Page 1
$feedEdge = $response->getGraphEdge();

foreach ($feedEdge as $status) {
  var_dump($status->asArray());
}

// Page 2 (next 5 results)
$nextFeed = $fb->next($feedEdge);

foreach ($nextFeed as $status) {
  var_dump($status->asArray());
}
 
 
 */


