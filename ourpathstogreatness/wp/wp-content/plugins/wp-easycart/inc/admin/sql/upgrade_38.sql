;
ALTER TABLE `ec_product` ADD `income_account_ref` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT 'Online Sales' COMMENT 'Quickbooks Specific account reference.';
ALTER TABLE `ec_product` ADD `cogs_account_ref` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT 'Cost of Goods Sold' COMMENT 'Quickbooks Specific account reference.';
ALTER TABLE `ec_product` ADD `asset_account_ref` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT 'Inventory Asset' COMMENT 'Quickbooks Specific account reference.';
ALTER TABLE `ec_tempcart_data` ADD `tempcart_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;