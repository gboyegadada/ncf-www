			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

                <div class="faux-shadow">&nbsp;</div>
                
				<div id="inner-footer" class="wrap cf">

                    <div class="d-3of5">
                    <div class="logo-symbol w100">&nbsp;</div>
                    
                    <?php optg_social_links(); ?>
                    
                    
					<nav role="navigation">
						<?php wp_nav_menu(array(
    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
    					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
    					'theme_location' => 'footer-links',             // where it's located in the theme
    					'before' => '',                                 // before the menu
    					'after' => '',                                  // after the menu
    					'link_before' => '',                            // before each link
    					'link_after' => '',                             // after each link
    					'depth' => 0,                                   // limit the depth of the nav
    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
						)); ?>
					</nav>

                    <p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>. All Rights Reserved. 
                        
                        <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Terms & Conditions' ) ) ); ?>"><?php esc_html_e( 'Terms & Conditions', 'textdomain' ); ?></a>
                        
                        <br />501(c)(3) tax-exempt charity
                    </p>
                    
                    <?php 
                    
                    
                    switch ($post->post_name) 
                    {
                        case 'who-we-are':
                            $q = "Until Lions have their own historians, tales of the hunt shall always glorify the hunter. ~ (Igbo proverb, Nigeria)";
                            break;
                        case 'what-we-do':
                            $q = "You must act as if it is impossible to fail. ~ Ashanti";
                            break;
                        case 'get-involved':
                            $q = "If you want to go quickly, go alone. If you want to go far, go together. ~ African proverb";
                            break;
                        case "donate":
                            $q = "If you want to go quickly, go alone. If you want to go far, go together. ~ African proverb";
                            break;
                        case "news": 
                            $q = "He who is destined for power does not have to fight for it. ~ Ugandan proverb";
                            break;
                        case "spotlight":
                            $q = "Food you will not eat you do not boil. ~African Proverb";
                            break;
                        
                        default:
                            $q = "Until Lions have their own historians, tales of the hunt shall always glorify the hunter. ~ (Igbo proverb, Nigeria)";
                            break;
                            
                        
                    }
                    
                    
                    ?>
                    </div>
                    
                    <div class="d-2of5">
                        <div class="footer-quote">
                            <div class="slider">
                                
                                <div>
                                    <p>
                                    <?= $q ?>
                                    </p>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    
				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
