<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
        <meta name="theme-color" content="#121212">
        
        
        
        
<!-- ****************** Start Dreamdare SEO ****************** -->	
<?php global $post;
	//Get assigned keywords
	$DD_meta_keywords = get_post_meta( $post->ID, '_cmb_dreamdare_meta_keywords', true );
	//Get assigned meta description
	$DD_meta_description = get_post_meta( $post->ID, '_cmb_dreamdare_meta_description', true );
	//Get search engine Meta Robot overwrite options
	$DD_follow_nofollow_select = get_post_meta( $post->ID, '_cmb_follow_nofollow_select', true );
	//Get the global keywords
	$global_keywords = get_option( 'DD_global_keywords', '' );
?>
<?php
//Get the WordPress global privacy setting and if it's blocking serach engines
		if ( '0' == get_option('blog_public') ){?>
		<meta name="robots" content="nofollow, noindex">
		<?php }else{
		?>
<?php
//Check if the user specified a robot request, if not, treat it as default (follow, index)
		if (empty($DD_follow_nofollow_select)) {?>
		<meta name="robots" content="follow, index">
		<?php } else{
//Check if the user specified a robot request, if yes, replace the default
		?>
			<meta name="robots" content="<?php echo $DD_follow_nofollow_select; ?>">
		<?php }} ?>

<?php
//If keyword is empty, echo global keywords set in the general setting
		if (empty($DD_meta_keywords)) {?>
		<meta name="keywords" content="<?php echo $global_keywords; ?>" />
		<?php } else{
//If the user has entered custom keywords, replace the global keywords
		?>
			<meta name="keywords" content="<?php echo $DD_meta_keywords; ?>" />
		<?php } ?>

<?php
//If meta description is empty, use the site description as meta description
		if (empty($DD_meta_description)) {?>
		<meta name="description" content="<?php bloginfo('description'); ?>" />
		<?php } else{
//If meta description is set by the user, replace the default description
		?>
			<meta name="description" content="<?php echo $DD_meta_description; ?>" />
		<?php } ?>
<!-- ****************** End of Dreamdare SEO ****************** -->
        



		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>
        
        <script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/math.uuid.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/library/js/libs/Utils.js"></script>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>
        

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<div id="container">

			<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader">


				<div id="inner-header" class="wrap cf">
                    

                    <div class="d-3of5 main-nav-wrap">
                        <nav id="main-nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                            <?php wp_nav_menu(array(
                                     'container' => false,                           // remove nav container
                                     'container_class' => 'menu cf',                 // class of container (should you choose to use it)
                                     'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
                                     'menu_class' => 'nav top-nav cf',               // adding custom nav class
                                     'theme_location' => 'main-nav',                 // where it's located in the theme
                                     'before' => '',                                 // before the menu
                                       'after' => '',                                  // after the menu
                                       'link_before' => '',                            // before each link
                                       'link_after' => '',                             // after each link
                                       'depth' => 2,                                   // limit the depth of the nav
                                     'fallback_cb' => ''                             // fallback function (if there is one)
                            )); ?>

                        </nav>
                    </div>
                    
                    

                    <div class="d-all mobile-nav-wrap">
                        <nav id="main-nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                            <?php wp_nav_menu(array(
                                     'container' => false,                           // remove nav container
                                     'container_class' => 'menu cf',                 // class of container (should you choose to use it)
                                     'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
                                     'menu_class' => 'nav top-nav cf',               // adding custom nav class
                                     'theme_location' => 'main-nav',                 // where it's located in the theme
                                     'before' => '',                                 // before the menu
                                       'after' => '',                                  // after the menu
                                       'link_before' => '',                            // before each link
                                       'link_after' => '',                             // after each link
                                       'depth' => 2,                                   // limit the depth of the nav
                                     'fallback_cb' => ''                             // fallback function (if there is one)
                            )); ?>


                            <?php optg_social_links() ?>
                            
                        </nav>
                        <div class="menu-toggle-button">
                            <span class="si-icon si-icon-hamburger-cross" data-icon-name="hamburgerCross"></span>
                        </div>
                    </div>
                    

					<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> ?>
                    
                    <div class="logo-wrap d-1of4">
                        <a href="<?php echo home_url(); ?>" rel="nofollow">
                            <div class="logo-symbol w400">&nbsp;</div>
                        </a>
                    </div>
                    
                    
                    
                    <div class="d-all">
                        
                        <div class="logo-text d-2of5">&nbsp;</div>
                        
                    </div>
					<?php // if you'd like to use the site description you can un-comment it below ?>
					<?php // bloginfo('description'); ?>
                    
                    <div class="d-all slogan-wrap">
                        
                        <div class="slogan"><span class="line1">Unique Paths.</span><span class="line2">Collective Greatness</span></div>

                        <?php optg_social_links() ?>
                    </div>

                    
                    
				</div>
                <div class="wrap cf">
                </div>

                    
                <div class="bars">
                
                    <div id="main-slider-nav-wrap" class="wrap cf">
                        <div class="slider-nav">
                        <ul>

                        </ul>
                        </div>
                    </div>


                    
                    
                    <hr class="wrap" />
                    <hr class="wrap" />
                </div>

			</header>
