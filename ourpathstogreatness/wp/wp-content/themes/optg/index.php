<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-2of3 d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<article id="post-" role="article">

								<section id="home-content-grid" class="entry-content cf">
									
                                    
                                    
                                    
									
                                    <div id="spotlights-tile" class="d-1of3">
                                        <div class="inner-wrap" style="background-image: url(/wp/wp-content/themes/optg/library/images/optg-1.jpg)">
           
                                            <section class="body">
                                                <a href="<?= esc_url( get_permalink( get_page_by_title( 'Spotlights' ) ) ) ?>">

                                                </a>
                                            </section>
                                 
                                            <header class="tile-header">
                                                <h1 class="h2 title">Spotlights</h1>
                                                <p class="text">Africans making positive impact within and outside the continent</p>
                                                <a class="anchor" href="<?= esc_url( get_permalink( get_page_by_title( 'Spotlights' ) ) ) ?>">Go to page</a>
                                            </header>
                                            
                                        </div>
                                    </div>
                                    <div class="d-1of3">
                                        <div class="inner-wrap" style="background-image: url(/wp/wp-content/themes/optg/library/images/optg-1.jpg)">

                                            <section class="body">

                                            </section>
                                            
                                            
                                            <header class="tile-header">
                                                <h1 class="h2 title">Who We Are</h1>
                                                <p class="text">Learn more about OPTG</p>
                                                <a class="anchor" href="<?= esc_url( get_permalink( get_page_by_title( 'Who We Are' ) ) ) ?>">Learn more</a>
                                            </header>
                                            
                                        </div>
                                    </div>
                                    <div class="d-1of3">
                                        <div class="inner-wrap" style="background-image: url(/wp/wp-content/themes/optg/library/images/optg-1.jpg)">

                                            <section class="body">

                                            </section>
                                            
                                            <header class="tile-header">
                                                <h1 class="h2 title">Get Involved</h1>
                                                <p class="text">OPTG Volunteer Signup Form</p>
                                                <a class="anchor" href="<?= esc_url( get_permalink( get_page_by_title( 'Get Involved' ) ) ) ?>">Click here to get started</a>
                                            </header>
                                            
                                        </div>
                                    </div>
                                    
                                                      
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <div id="gallery-tile" class="d-1of3">
                                        <div class="inner-wrap" style="background-image: url(/wp/wp-content/themes/optg/library/images/optg-1.jpg)">

                                            <section class="body">

                                            </section>
                                            
                                            <header class="tile-header">
                                                <h1 class="h2 title">Photos</h1>
                                                <p class="text">See our impact</p>
                                                <a class="anchor" href="<?= esc_url( get_permalink( get_page_by_title( 'Photos' ) ) ) ?>">Go to photo page</a>
                                            </header>
                                            
                                        </div>
                                    </div>
                                    <div class="d-1of3">
                                        <div class="inner-wrap" style="background-image: url(/wp/wp-content/themes/optg/library/images/optg-1.jpg)">

                                            <section class="body">

                                            </section>
                                            
                                            
                                            <header class="tile-header">
                                                <h1 class="h2 title">Videos</h1>
                                                <p class="text">See what we're about</p>
                                                <a class="anchor" href="<?= esc_url( get_permalink( get_page_by_title( 'Videos' ) ) ) ?>">Visit our YouTube page</a>
                                            </header>
                                            
                                        </div>
                                    </div>
                                    <div class="d-1of3">
                                        <div class="inner-wrap" style="background-image: url(/wp/wp-content/themes/optg/library/images/optg-1.jpg)">

                                            <section class="body">

                                            </section>
                                            
                                            
                                            <header class="tile-header">
                                                <h1 class="h2 title">Donate</h1>
                                                <p class="text">Help support our work</p>
                                                <a class="anchor" href="<?= esc_url( get_permalink( get_page_by_title( 'Donate' ) ) ) ?>">Donate now</a>
                                            </header>
                                            
                                        </div>
                                    </div>
                                    

                                    
                                    
								</section>
                                
								<footer class="article-footer cf">
                                    

								</footer>
                                
                                <p>&nbsp;</p>

							</article>

						</main>

				</div>

			</div>

<script type="text/javascript">   
    jQuery(document).ready(function ($) { 
<?php 

// 1. Facebook slider
$feed = fbgroupfeed(['group_id'=>"788904837871778", 'count'=>5, 'print'=>0]); 

if (is_array($feed)) {
    echo 'var fbslides = ['."\n";
    foreach ($feed as $fbpost) 
    {
        if (!isset($fbpost['full_picture']) || empty($fbpost['full_picture'])) continue;
        echo '"' . urldecode($fbpost['full_picture']) . '", '."\n";
    }
    echo "\n".']; $("#spotlights-tile > .inner-wrap").backstretch(fbslides, {duration: 3000, fade: 750});';
}

// 2. Photo gallery
$my_wp_query = new WP_Query();
$all_wp_pages = $my_wp_query->query(array('post_type' => 'page'));

// Get the page as an Object
$photos_index_pg =  get_page_by_title('Photos');

// Filter through all pages and find Portfolio's children
$photo_pages = get_page_children( $photos_index_pg->ID, $all_wp_pages );


echo "\n\n".'var gslides = ['."\n";
foreach ($photo_pages as $ppg) 
{
    if ( !has_post_thumbnail($ppg->ID) ) continue;
    echo '"' . wp_get_attachment_image_src( get_post_thumbnail_id( $ppg->ID), [ 500, 500 ])[0] . '", '."\n";
}
echo "\n".']; $("#gallery-tile > .inner-wrap").backstretch(gslides, {duration: 3100, fade: 750});';



// 3. YouTube Channel

//$DEVELOPER_KEY = 'AIzaSyBx2mvM_sp2uJ9fZdkmAITHnZtloBL7Pio';
//file_get_content("https://www.googleapis.com/youtube/v3/search?key=".$DEVELOPER_KEY."&channelId={channel_id_here}&part=snippet,id&order=date&maxResults=4");



?>
         });
 </script>
 
 
<?php get_footer(); ?>
