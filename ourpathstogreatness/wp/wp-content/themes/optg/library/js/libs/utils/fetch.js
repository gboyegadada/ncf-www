
// Class: [ fetch ]
;(function (m) { 
    
    

    m.fetch = function (url, options, keychain) {

        var 

        fetch = this, 
        callBack = {
        success: function (response) {}, 
        error: function (xhr) {
            throw new Exception ('Connection failed: ' + xhr.responseText());

            }
        },
        defaultOptions = {
          method: 'GET', 
          async: true, 
          withCredentials: true, 
          headers: [] , 
          dataType: 'html', 
          contentType: 'application/x-www-form-urlencoded',
          data: null
        };


        if (typeof options.type === 'string' && typeof options.method === 'undefined') {
            options.method = options.type;
        }

        options = m.extend(defaultOptions, options);


        options.method = options.method.toLowerCase();

        if (typeof options.data !== 'string' && options.data !== null) {
            if (
                    (typeof options.data.whisper === 'undefined' || options.data.whisper === '') 
                    && typeof keychain !== 'undefined' 
                    && typeof keychain.whisper === 'string') options.data.whisper = keychain.whisper;
            options.data = (options.data.nodeName === "FORM") ? serialize(options.data) : m.param(options.data);
        }

        if (options.method === 'get' && typeof options.data === 'string')  {
            url += '?' + options.data;
            options.data = null;
        }

        if (options.method === 'post')  {
            options.headers.push({'name': 'Content-type', 'value': 'application/x-www-form-urlencoded;charset=UTF-8' });
        }

        options.headers.push({'name': 'X-Requested-With', 'value':  'XMLHttpRequest'});
        //options.headers.push({'name': 'X-Cataleya-Auth-Token', 'value':  _auth_token});
        //options.headers.push({'name': 'X-Cataleya-Finger-Print', 'value':  _fp});
        if (typeof keychain !== 'undefined') options.headers.push({'name': 'X-Cataleya-Request-Token', 'value':  keychain.rt});




        var core = {

            ajax: function (url, options, cb) {


                               var 

                               xhr = getXHR(),  



                               // Event Listeners...

                               transferComplete = function transferComplete (e) {

                                   var status = (xhr.status === 1223) ? 204 : xhr.status;
                                   if (status < 100 || status > 599) {
                                     if (typeof options.error === 'function') cb.error(xhr);
                                     return;
                                   }

                                   if (status === 200) {


                                        var responseObj = {

                                            json: function () {

                                                try {
                                                    var data = JSON.parse(xhr.responseText.trim());
                                                    return data;
                                                } catch (e) {
                                                    throw new Error (xhr.responseText);
                                                }

                                            }, 
                                            xml: function( data ) {
                                                    var xml, tmp;
                                                    if ( !data || typeof data !== "string" ) {
                                                            return null;
                                                    }
                                                    try {
                                                            if ( window.DOMParser ) { // Standard
                                                                    tmp = new DOMParser();
                                                                    xml = tmp.parseFromString( data , "text/xml" );
                                                            } else { // IE
                                                                    xml = new ActiveXObject( "Microsoft.XMLDOM" );
                                                                    xml.async = "false";
                                                                    xml.loadXML( data );
                                                            }
                                                    } catch( e ) {
                                                            xml = undefined;
                                                    }
                                                    if ( !xml || !xml.documentElement || xml.getElementsByTagName( "parsererror" ).length ) {
                                                            throw new Error ( "Invalid XML: " + data );
                                                    }
                                                    return xml;
                                            },
                                            responseText: function () {
                                                return xhr.responseText;
                                            }, 
                                            status: function () {
                                                return xhr.statusText;
                                            }

                                        };


                                        if (typeof cb.success === 'function') cb.success(responseObj, xhr.status, xhr);



                                   }
                                   else if (xhr.status !== 200) {

                                       if (typeof cb.error === 'function') cb.error(xhr);

                                   }
                               },


                               transferFailed = function transferFailed (e) {
                                 if (typeof cb.error === 'function') cb.error(xhr);
                               }, 



                               transferCancelled = function transferCancelled (e) {
                                 if (typeof cb.cancelled === 'function') cb.cancelled(xhr);
                               };





                               xhr.addEventListener("load", transferComplete, false);
                               xhr.addEventListener("error", transferFailed, false);
                               xhr.addEventListener("abort", transferCancelled, false);
                               xhr.withCredentials = options.withCredentials;



                               // Open connection...
                               xhr.open(options.method, url, options.async);

                               // Set headers...
                               options.headers.forEach(function (h) {
                                   xhr.setRequestHeader(h.name, h.value);
                               });



                               if (typeof cb.beforeSend === 'function') cb.beforeSend(xhr);


                               xhr.send(options.data);
                       }
                };




            fetch.then = function (cb) {
                    if (typeof cb === 'function') callBack.success = cb;

                    return fetch;
                };

            fetch.catch = function (cb) {
                if (typeof cb === 'function') callBack.error = cb;

                return fetch;
            };

            setTimeout(function () {
                core.ajax(url, options, callBack);
            }, 0);

            return fetch;




    };
    
    
    


    function getXHR () {
            var XMLHttpObject = null;


            // Firefox, Opera 8+, Safari...

            try 
                    { XMLHttpObject = new XMLHttpRequest();		}

            catch (e)
                    {

                    // Internet Explorer...
                    try
                            { XMLHttpObject = new ActiveXObject ("Msxml2.XMLHTTP"); }


                    catch (e)
                            {
                            XMLHttpObject = new ActiveXObject ("Microsoft.XMLHTTP");

                            }

                    }



          return XMLHttpObject;
    };




    function serialize (form) {
            if (!form || form.nodeName !== "FORM") {
                    return;
            }
            var i, j, q = [];
            for (i = form.elements.length - 1; i >= 0; i = i - 1) {
                    if (form.elements[i].name === "") {
                            continue;
                    }
                    switch (form.elements[i].nodeName) {
                    case 'INPUT':
                            switch (form.elements[i].type) {
                            case 'text':
                            case 'hidden':
                            case 'password':
                            case 'button':
                            case 'reset':
                            case 'submit':
                                    q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                                    break;
                            case 'checkbox':
                            case 'radio':
                                    if (form.elements[i].checked) {
                                            q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                                    }						
                                    break;
                            }
                            break;
                            case 'file':
                            break; 
                    case 'TEXTAREA':
                            q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                            break;
                    case 'SELECT':
                            switch (form.elements[i].type) {
                            case 'select-one':
                                    q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                                    break;
                            case 'select-multiple':
                                    for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
                                            if (form.elements[i].options[j].selected) {
                                                    q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));
                                            }
                                    }
                                    break;
                            }
                            break;
                    case 'BUTTON':
                            switch (form.elements[i].type) {
                            case 'reset':
                            case 'submit':
                            case 'button':
                                    q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                                    break;
                            }
                            break;
                    }
            }
            return q.join("&");
    }





    function handleData (dataType, data) {

        if (typeof data === 'string') return data;
        else return m.param(data);

        switch (dataType) {
            case 'form': 

                return m.param(data);

            break;

            case 'json': 

                return JSON.stringify(data);

            break;

            case 'query': 

                return m.param(data);

            break;

            case 'text': 

                return encodeURI(data);

            break;

            case 'plain': 

                return encodeURI(data);

            break;

            case 'plain-text': 

                return encodeURI(data);

            break;

            default:

                return m.param(data);

            break;

        }


    };



    
    
    
    
    
 return m;
 
}(Utils));
