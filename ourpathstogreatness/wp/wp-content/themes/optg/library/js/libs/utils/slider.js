
 
// ----------------------------------------------------------------------------------------- //


// Class: [ makeSlider ]
;(function (m) {   
    
    
    
    m.Slider = function (_wrapper, _transition, _view_port) {
    
    
    // Private properties
    
    //main._unseal();
    //main._private.someThing = 'blah';
    //main._seal();
    
    var 
    
    self = this, 
    env = {
        uid: Math.uuid(5), 
        frames: {}, 
        frame_history: -1, 
        frame_count: 0, 
        frame_width: 0, 
        frame_pos: 0, 
        slide_duration: 300, 
        view_Port: 1,
        title_selector: null, 
        css: {
            frame: {}, 
            front: {}, 
            transit: {}
        }, 
        event_detail: {
            self: self
        }, 
        interval: null, 
        winResizeTimeout: null
    },  
    type_of = function (obj) {
    
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    };
    
    
    var 
    
    _eventsRegister = {
        'firstFrame':[], 
        'lastFrame':[], 
        'nextFrame':[], 
        'previousFrame':[], 
        'seekFrame':[], 
        'change':[],
        'loadingComplete':[]
        },
    
            
    onFirstFrame = new CustomEvent('firstFrame' + env.uid, { 
                                            detail: env.event_detail,
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
    onLastFrame = new CustomEvent('lastFrame' + env.uid, { 
                                            detail: env.event_detail,
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
    onNextFrame = new CustomEvent('nextFrame' + env.uid, { 
                                            detail: env.event_detail,
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
    onPreviousFrame = new CustomEvent('previousFrame' + env.uid, { 
                                            detail: env.event_detail,
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
    onSeekFrame = new CustomEvent('seekFrame' + env.uid, { 
                                            detail: env.event_detail,
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
    onChange = new CustomEvent('change' + env.uid, { 
                                            detail: env.event_detail,
                                            bubbles: true,
                                            cancelable: false 
                                        }),
                                        
    onLoadingComplete = new CustomEvent('loadingComplete' + env.uid, { 
                                            detail: env.event_detail,
                                            bubbles: true,
                                            cancelable: false 
                                        });
    
    
    
    var _strip = m.dom('<div class="strip"></div>').wrapAll(_wrapper.children);
    
    
    env.frames = _wrapper.querySelectorAll('.strip > *');
    m.all(env.frames).addClass('frame');
    m.one(_wrapper).css({
        // position: 'relative', 
        // display: 'block', 
        // width: '100%', 
        // height: 'inherit', 
        overflow: 'hidden'
    });
    
    env.frame_strip = _strip.item(0);
    env.frame_count = env.frames.length;
    env.view_Port = (typeof _view_port !== 'number' || _view_port > env.frame_count) ? 1 : parseInt(_view_port);
    
    _initSlider ();
    
    env.title_selector = _wrapper.getAttribute('data-title-selector');
    env.title_selector = (typeof env.title_selector === 'string' && env.title_selector.length > 0) ? m.all(document.querySelector(env.title_selector)) : null;
    
    
    window.addEventListener('resize', _winResizeHandler);
    
    // Load moustache templates
    //ajax.loadTemplates(['layout-dialog', 'layout-tile-dialog', 'layout-tile', 'layout-slide-dialog'], true);
    
    var efx = {
        none: function (slidePos, callBack) {
             
            var pixelPos = 0;
            pixelPos = 0-(slidePos*env.frame_width);
            
            // $(env.frame_strip).animate({left: pixelPos}, env.slide_duration);
            var s = m.one(env.frame_strip);
            
            s.css(env.css.mute);
            
            env.frame_strip.style.left = pixelPos + 'px';
            env.frame_strip.offsetHeight;
            
            s.css(env.css.frame_strip);
           
            env.frame_pos = slidePos;
            
            efx.refreshTitle();
            
            env.event_detail = { uid: env.uid, index: env.frame_pos, population: env.frame_count };
            
            if (typeof callBack === 'function') callBack();
        }, 
        fade: function (slidePos, callBack) {
            
            
            if (env.frame_history > -1) m.all(env.frames[env.frame_history]).removeClass('front').css(env.css.reset);

            m.all(env.frames[slidePos]).addClass('front').css(env.css.front);
           
            env.frame_pos = slidePos;
            
            efx.refreshTitle();
            
            env.event_detail = { uid: env.uid, index: env.frame_pos, population: env.frame_count };
            
            if (typeof callBack === 'function') callBack();
        }, 
        
        slide: function (slidePos, callBack) {
            
            
            var pixelPos = 0;
            pixelPos = 0-(slidePos*env.frame_width);
            
            // $(env.frame_strip).animate({left: pixelPos}, env.slide_duration);
            
            env.frame_strip.style.left = pixelPos + 'px';
           
            env.frame_pos = slidePos;
            
            efx.refreshTitle();
            
            env.event_detail = { uid: env.uid, index: env.frame_pos, population: env.frame_count };
            
            if (typeof callBack === 'function') callBack();
        }, 
        
        refreshTitle: function () {
            
            if (env.title_selector !== null) {
                
                env.title_selector.fadeOut('inline-block', function (el) {
                    
                    el.item(0).innerHTML = env.frames[env.frame_pos].getAttribute('data-title');
                    
                    
                    el.fadeIn('inline-block');
                });
                
            }
            
            
        }
    };






    function _initSlider () {
            
            var _computedFrameWidth = parseFloat(window.getComputedStyle(_wrapper).getPropertyValue("width")) / env.view_Port;

            
            for (var k=0; k<env.frames.length; k++) { 
                env.frames[k].style['width'] = _computedFrameWidth+'px';
            }
            
            env.frame_strip.style['width'] = (_computedFrameWidth*env.frame_count) + 'px';

            env.frame_width = _computedFrameWidth; // parseFloat(window.getComputedStyle(env.frames[0]).getPropertyValue("width"));
            
            
            var pixelPos = pixelPos = 0-(env.frame_pos*env.frame_width);
            
            env.frame_strip.style.left = pixelPos + 'px';
            
            
        }
        
        
        
    function _winResizeHandler (e) {
        clearTimeout(env.winResizeTimeout);
        env.winResizeTimeout = setTimeout(_initSlider, 1500);
    }





    
    
    _transition = (typeof _transition === 'String' && ['fade', 'slide'].indexOf(_transition) > -1) ? 'fade' : _transition;
    
    switch (_transition) {
        case 'fade':
            env.css = {
                
                frame: {
                    display: 'block', 
                    position: 'absolute',  
                    height: 'inherit', 
                    padding: '0 0', 
                    margin: '0 0', 
                    cssFloat: 'none',
                    background: 'none',  
                    opacity: '0', 
                    'z-index': '10', 
                    transition: 'all 0.5s ease-in-out', 
                    WebkitTransition: 'all 0.5s ease-in-out', 
                    MozTransition: 'all 0.5s ease-in-out'
                },
                reset: {
                    opacity: '0', 
                    'z-index': '10'
                },
                
                front: {
                    opacity: '1', 
                    'z-index': (env.frame_count + 20)+''
                }, 
                
                frame_strip: {
                    position: 'absolute', 
                    display: 'block', 

                    height: 'inherit',

                    'white-space': 'nowrap',
                    'text-align': 'left',

                    margin: '0 0', 
                    padding: '0 0', 
                    clear: 'both'
                }
            };
            break;
            
        case 'slide':
            env.css = {
                
                frame: {
                    display: 'block', 
                    // width: '438px', 
                    height: 'inherit', 

                    padding: '0 0', 
                    margin: '0 0', 

                    background: 'none', 
                    position: 'relative', 
                    cssFloat: 'left', 
                    opacity: '1'
                }, 
                
                frame_strip: {
                    position: 'absolute', 
                    display: 'block', 

                    height: 'inherit',

                    'white-space': 'nowrap',
                    'text-align': 'left',

                    margin: '0 0', 
                    padding: '0 0', 
                    clear: 'both', 
                    transition: 'all 0.5s ease-in-out', 
                    WebkitTransition: 'all 0.5s ease-in-out', 
                    MozTransition: 'all 0.5s ease-in-out'
                }
                
            };
            break;
            
    }
    
    env.css.mute = {
                    transition: 'none', 
                    WebkitTransition: 'none', 
                    MozTransition: 'none'
                };
    
    
    for (var i=0; i<env.frames.length; i++) {
        m.all(env.frames[i]).css(env.css.frame);
        
    }
    
    m.all(env.frame_strip).css(env.css.frame_strip);
    
    
    
    // on
    self.on = function (event, func) {

        if (typeof _eventsRegister[event] === 'undefined' || typeof func !== 'function') return false;
        
        document.addEventListener(event+env.uid, func);
        _eventsRegister[event].push(func);
        
        return self;
    };     
    
    
    
    // off
    self.off = function (event, func) {

        if (typeof _eventsRegister[event] === 'undefined' || typeof func !== 'function') return false;
        
        document.removeEventListener(event+env.uid, func);
        
        _eventsRegister[event].forEach(function (f, i) {
            if(f.name === func.name) _eventsRegister[event].slice(i, 1);
        });
        
        return self;
    };    
    
    

    self.destroy = function  () {
        
        for (var event in _eventsRegister) {
            

            _eventsRegister[event].forEach(function (f) {
                
                document.removeEventListener(event+env.uid, f);
                
            });
            
            window.removeEventListener('resize', _winResizeHandler);
            
        }
        
        _eventsRegister = null;
    };
    
    
    
    /*
     * 
     * [ FUNCTION ] nextSlide
     * ____________________________________________________________________________
     * 
     * 
     * 
     */


     self.gotoSlide = function (slidePos, callBack, muteEfx) {
            
            if (typeof slidePos !== 'number') { throw new TypeError ('Invalid slide position!'); return; }
            muteEfx = (typeof muteEfx !== 'boolean') ? false : muteEfx;
            
            
            
            slidePos = parseInt(slidePos);
            
            if (slidePos > env.frame_count-env.view_Port) return;
            env.frame_history = env.frame_pos;
            
            
            
            efx[(muteEfx)?'none':_transition](slidePos, function () {
                
                if (typeof callBack === 'function') callBack();
                document.dispatchEvent(onSeekFrame);
                document.dispatchEvent(onChange);
                
            });
                    
                    

            
           
            
            return;
     };
     
     
     
     
     
    /*
     * 
     * [ FUNCTION ] nextSlide
     * ____________________________________________________________________________
     * 
     * 
     * 
     */
    
    self.back = function  () {
        if (env.frame_history != env.frame_pos) self.gotoSlide(env.frame_history, function () {
                document.dispatchEvent(onPreviousFrame);
            });
    };
    
    
    

    
    
    /*
     * 
     * [ FUNCTION ] nextSlide
     * ____________________________________________________________________________
     * 
     * 
     * 
     */


     self.nextSlide = function (rewind) {
            rewind = (typeof rewind !== 'boolean') ? false : rewind;
            
            var 
            

            pixelPos = 0, 
            slidePos = env.frame_pos;
            env.frame_history = env.frame_pos;
            

            if (slidePos >= env.frame_count-env.view_Port) { 
                return (!rewind) ? false : self.gotoSlide(0);
            }
            slidePos++;

            
            efx[_transition](slidePos, function () {
                document.dispatchEvent(onNextFrame);
                document.dispatchEvent(onChange);
            });
            
            return;
     };
     
     
     
     
     
     

    
    
    /*
     * 
     * [ FUNCTION ] prevSlide
     * ____________________________________________________________________________
     * 
     * 
     * 
     */


     self.prevSlide = function () {
            
            var 

            pixelPos = 0, 
            slidePos = env.frame_pos;
            env.frame_history = env.frame_pos;
            

            if (slidePos == 0) return;

            slidePos--;
            

            efx[_transition](slidePos, function () {
                document.dispatchEvent(onPreviousFrame);
                document.dispatchEvent(onChange);
            });
            
            return;
     };
    
    

   
     self.play = function (delay, rewindBefore) {
         delay = (typeof delay !== 'number') ? 5000 : parseFloat(delay);
         rewindBefore = (typeof rewindBefore !== 'number') ? env.frame_count : parseInt(rewindBefore);
         
         self.stop();
         
         env.interval = setInterval(function () { self.nextSlide(true); }, delay);
     };
     
     
     self.stop = function () {
         if (env.interval) clearInterval(env.interval);
     };
     
     self.pause = self.stop;
   
    
     self.getUID = function () {
         return env.uid;
     };
    
    
     self.getPos = function () {
         return env.frame_pos;
     };
     
     
     self.getPosition = self.getPos;
    
    
     self.getLastPos = function () {
         return env.frame_history;
     };
    
    
     self.getPopulation = function () {
         return env.frame_count;
     };
    




    

    
    self.gotoSlide(0);
    
    return self;
    
    
    
    
 };
 
 return m;
 
}(Utils));
