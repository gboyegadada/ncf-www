
       
<?php





$idObj = get_category_by_slug('actor-du-jour'); 

$args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 1);
$myposts = get_posts( $args );

if ( !empty($myposts) ) : 


      
      $post = $myposts[0];
      setup_postdata($post);
      
      $long = trim(strip_tags(get_the_content( '' ), ''));
      $short = (strlen($long) > 180) ? substr($long,0,180) : $long;
        
      $attachments = get_attached_media( 'image',  $post->ID );
      
      foreach ($attachments as $image) {
        $image_url = ( $image ) ? $image->guid : "";
        break;
      }
      
        

      ?>


    <div class="actor-du-jour clearfix">    

        
            <div class="photo"  style="background: url('<?= $image_url ?>') no-repeat center center; background-size: cover;">&nbsp;</div>
            
            <div class="star"><span class="glyphicon glyphicon-star blink-2"></span></div>
            
            <div class="title"><?= get_the_title(); ?></div>
            <div class="body">
                
                <div class="short">
                    <?= $short ?>
                </div>
                
                <div class="long">
                    <?= $long ?>
                </div>
                

                <br />
                <a class="expand-button"  href="javascript:void(0);" onclick="readMore();" >Read more</a>
            
            </div>
            
     </div>

     
<?php endif; ?>


          
   