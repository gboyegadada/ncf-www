<?php 
define('WP_USE_THEMES', false);
require('../../news/wp-blog-header.php');

?>




<div class="ajax-content-header">
    Giving Back
    
</div>

<hr>

<div class="ajax-content-body">
    
    
<?php


$idObj = get_category_by_slug('community'); 

$args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 5);
$myposts = get_posts( $args );

if ( !empty($myposts) ) : 



    /* The loop */ 

    foreach( $myposts as $post ) : 

      setup_postdata($post);
      $content = get_the_content( '' );

      ?>
    


        <!-- Carousel 
        ================================================== -->
        <div id="myCarousel" class="carousel fade col-md-6" >

          <div class="carousel-inner">


                <?php



                            $args = array( 
                                'post_type' => 'attachment', 
                                'post_mime_type' => 'image',
                                'numberposts' => -1, 
                                'post_status' => null, 
                                'post_parent' => $post->ID 
                            ); 

                            $attached_images = get_posts( $args );
                            $_active = true;


                            // Loop through each image in each gallery
                            foreach( $attached_images as $image ) {


                                        $imgData = wp_get_attachment_image_src( $image->ID, 'large', false );

                                        ?>

                                        <div class="item <?php if ($_active) { echo 'active'; $_active = false; } ?>">
                                            <div class="slide-img" style="background: url('<?= $imgData[0] ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; ">&nbsp;</div>

                                            <div class="container">

                                            <div class="carousel-caption col-md-12">
                                                <p class="col-md-12">
                                                    <?php // print_r ($imgData); ?>
                                                </p>

                                              <div class="bg">&nbsp;</div>
                                            </div>

                                          </div>
                                        </div>


                                          <?php

                                        }


                                        ?>




            </ul>         



        </div>

        </div>



        <div class="blurb">

                    <?= $content ?>

        </div>


<?php 


endforeach; 


else : 


    ?>

        <!-- No reviews yet -->
<?php endif; ?>









</div>




<script type="text/javascript">

                // $(".ajax-content-header").stick_in_parent();

                $('.carousel').carousel({
                    interval: 5000, 
                    pause: false

                });


</script>