<?php 
define('WP_USE_THEMES', false);
require('../../news/wp-blog-header.php');









nocache_headers();
?>




<div class="ajax-content-header">
    Gallery
    
</div>

<hr>

<div class="ajax-content-body">


              
       
<?php


$idObj = get_category_by_slug('main-gallery'); 

$args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 1);
$myposts = get_posts( $args );

if ( !empty($myposts) ) : 



    /* The loop */ 

    foreach( $myposts as $post ) : 

      setup_postdata($post);
      $content = get_the_content( '' );

      

    // Retrieve all galleries of this post
    $gallery = get_post_gallery( $post, false );
    $ids = explode(',', $gallery['ids']);
    
    

      ?>


    <div class="galleria">    
            <?php

            // Loop through each image in each gallery
            foreach( $ids as $image ) {
                $image_obj = get_post($image);

            ?>
        
            <img src="<?= wp_get_attachment_image_src( intval($image), 'large', false)[0] ?>" data-description="<?= $image_obj->post_content ?>">
            
            
            
            <?php  }  ?>
            
        </div>

          

<?php 


break;
endforeach; 


else : 


    ?>

        <!-- No reviews yet -->
<?php endif; ?>


          
        

</div>




<script type="text/javascript">
    
    Galleria.loadTheme('js/vendor/galleria/themes/classic/galleria.classic.min.js');
    Galleria.run('.galleria', {
    imageCrop: false
    });
    
</script>