<?php 
define('WP_USE_THEMES', false);
require('../../news/wp-blog-header.php');

?>




<div class="ajax-content-header">
    News
    
</div>


<div class="ajax-content-body">


    <ul class="news-wrap">
        <?php query_posts('cat=1&showposts=15'); ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <li class="news-item">
            
            <a href="<?php the_permalink(); ?>" target="_blank">
                <div class="title"><?php the_title(); ?></div>
            </a>
            
            <div class="date" ><?= get_the_date('j F Y'); ?>  </div>
            
            <div class="body">
                <?php the_content( 'Continue reading ...' ); ?>
                
             </div>
            
            <div class="social">
                <!-- Facebook -->
                <div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
            
            </div>
            <div class="admin">
                <?php edit_post_link( 'Edit', '<br /><br />', '' ); ?>
            </div>
        
        </li>
        <?php endwhile; else: ?>
        <?php endif; ?>

    </ul>         
                

</div>

