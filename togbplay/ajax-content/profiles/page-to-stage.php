<?php 
define('WP_USE_THEMES', false);
require('../../news/wp-blog-header.php');

?>


<div class="ajax-content-header">
    Page To Stage
</div>

<hr>

<div class="ajax-content-body">


              
       

    <div class="buttons-wrap col-md-6">
        <div class="bg">&nbsp;</div>
        
        
        
            <a class="video-thumb-anchor" href="javascript:void(0);" onclick="watchYoutubeVideo('uLwiHYhi5xw', 'TOGB Stage Play Premiere - Short Interviews');">
            <div id="vt004" class="video-thumb">
                <div class="bg" style="background: url(http://img.youtube.com/vi/uLwiHYhi5xw/0.jpg) no-repeat; background-size: cover;">&nbsp;</div>
                <div class="label">TOGB Stage Play Premiere - Short Interviews</div>
            </div>
            </a>
        

            <a class="video-thumb-anchor" href="javascript:void(0);" onclick="watchYoutubeVideo('hPlcuRBDJAo', 'TOGB Stage Play Trailer');">
            <div id="vt005" class="video-thumb">
                <div class="bg" style="background: url(http://img.youtube.com/vi/hPlcuRBDJAo/0.jpg) no-repeat; background-size: cover;">&nbsp;</div>
                <div class="label">TOGB Stage Play Trailer</div>
            </div>
            </a>
        

        
            <a class="video-thumb-anchor" href="javascript:void(0);" onclick="watchYoutubeVideo('7mdlk3ruU8c', 'Thread of Gold Beads Voiceover Session');">
            <div id="vt006" class="video-thumb">
                <div class="bg" style="background: url(http://img.youtube.com/vi/7mdlk3ruU8c/0.jpg) no-repeat; background-size: cover;">&nbsp;</div>
                <div class="label">Thread of Gold Beads Voiceover Session</div>
            </div>
            </a>
        
            <a class="video-thumb-anchor" href="javascript:void(0);" onclick="watchYoutubeVideo('3LVJCqUVpRQ', 'Impact Africa: September 28, 2014 - African Literature and Transformation');">
            <div id="vt007" class="video-thumb">
                <div class="bg" style="background: url(http://img.youtube.com/vi/3LVJCqUVpRQ/0.jpg) no-repeat; background-size: cover;">&nbsp;</div>
                <div class="label">Impact Africa: September 28, 2014 - African Literature and Transformation</div>
            </div>
            </a>
        

            <a class="video-thumb-anchor" href="javascript:void(0);" onclick="watchYoutubeVideo('3rWPhfVBOiE', 'Book Trailer');">
            <div id="vt001" class="video-thumb">
                <div class="bg" style="background: url(http://img.youtube.com/vi/3rWPhfVBOiE/0.jpg) no-repeat; background-size: cover;">&nbsp;</div>
                <div class="label">Book Trailer</div>
            </div>
            </a>
        
        
            <a class="video-thumb-anchor" href="javascript:void(0);" onclick="watchYoutubeVideo('gnPQClbcn8Q', 'Channels TV PT1');">
            <div id="vt002" class="video-thumb">
                <div class="bg" style="background: url(http://img.youtube.com/vi/gnPQClbcn8Q/0.jpg) no-repeat; background-size: cover;">&nbsp;</div>
                <div class="label">Channels TV PT1</div>
            </div>
            </a>
        
        
            <a class="video-thumb-anchor" href="javascript:void(0);" onclick="watchYoutubeVideo('_ZG1AYyqJDQ', 'Channels TV PT2');">
            <div id="vt003" class="video-thumb">
                <div class="bg" style="background: url(http://img.youtube.com/vi/_ZG1AYyqJDQ/0.jpg) no-repeat; background-size: cover;">&nbsp;</div>
                <div class="label">Channels TV PT2</div>
            </div>
            </a>
            
        
            <a class="video-thumb-anchor" href="javascript:void(0);" onclick="watchYoutubeVideo('SAUjCk_lEXI', 'Live with the Humble Prince');">
            <div id="vt004" class="video-thumb">
                <div class="bg" style="background: url(http://img.youtube.com/vi/SAUjCk_lEXI/0.jpg) no-repeat; background-size: cover;">&nbsp;</div>
                <div class="label">Live with the Humble Prince</div>
            </div>
            </a>

        
    </div>
    
    
    
    
    

    <div class=" col-md-5 pull-right">  
                    
                    <div id="splash-001" class="tile-content-wrapper" >
                    <ul class="slide-strip" >
                        
                    <?php

                      $idObj = get_category_by_slug('reviews'); 

                      $args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 5);
                      $myposts = get_posts( $args );

                      if ( !empty($myposts) ) : ?>

                              <?php /* The loop */


                              ?>
                              <?php foreach( $myposts as $post ) : setup_postdata($post); ?>
                                    
                        <li class="slide" >

                        <div class="slide-content-wrapper" style="background: none; " >
                            
                            <?= get_the_content( '' ); ?>
                            
                        </div>

                        </li>
                        
                        

                                <?php endforeach; ?>


                        <?php else : ?>
                                
                                <!-- No reviews yet -->
                        <?php endif; ?>
                        


                    </ul>
                    </div>
    </div>
                    
          

    
          
        

</div>






<!-- Modal: MAIN -->
<div class="modal fade" id="MainModal" tabindex="-1" role="dialog" aria-labelledby="MainModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          
          
        <button type="button" class="close" data-dismiss="modal">Close <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="MainModalLabel">Live with the Humble Prince</h4>
        
        

        <!-- Facebook -->
        <div class="fb-like" data-href="http://www.youtube.com/embed/SAUjCk_lEXI" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
           
      </div>
      <div class="modal-body">
        <iframe width="640" height="360" src="http://www.youtube.com/embed/SAUjCk_lEXI" frameborder="0" allowfullscreen></iframe>
      </div>

    </div>
  </div>
</div>










<script type="text/javascript">

var slider01;

slider01 = makeSlideshow($('#splash-001').find('.slide-strip'), {duration: 200, interval: 10000, transition: 'fade'});

slider01.play(); 






</script>