<?php 
define('WP_USE_THEMES', false);
require('../../news/wp-blog-header.php');

nocache_headers();
?>



    <?php

    $idObj = get_category_by_slug('quotes'); 

    remove_all_filters('posts_orderby');
    $args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 1, 'orderby' => 'rand');
    $myposts = get_posts( $args );

    if ( !empty($myposts) ) : ?>

            <?php /* The loop */


            ?>
           <?php foreach( $myposts as $post ) : setup_postdata($post); ?>

            <p class="col-md-9 pull-left">
                  <?= strip_tags(get_the_content( '' ), ''); ?>
                
            </p>
            
            <?php
            
            $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
            
            ?>
            
            <div class="quote-image" style="background: url('<?php echo $large_image_url[0]; ?>') no-repeat bottom center; background-size: contain;"></div>
            
            <?php endforeach; ?>

    <?php endif; ?>
    
    
