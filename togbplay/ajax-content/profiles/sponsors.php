<?php 
define('WP_USE_THEMES', false);
require('../../news/wp-blog-header.php');









nocache_headers();
?>




<div class="ajax-content-header">
    Sponsors
    
</div>

<hr>

<div class="ajax-content-body">


              
       
<?php




$idObj = get_category_by_slug('sponsors'); 

$args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 20);
$myposts = get_posts( $args );

if ( !empty($myposts) ) : 

?>
    
    

    <div class="sponsors-main-wrap clearfix">  
        
    <?php

    /* The loop */ 

    foreach( $myposts as $post ) : 

      setup_postdata($post);

      
        $image_url = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ), 'medium' );


      ?>

        <div class="row">
            <div class="col-md-4">
            <div class="sponsors-logo" style="background: #fff url('<?= $image_url ?>') no-repeat center center; background-size: contain;">&nbsp;</div>
            </div>
            
            
            <div class="col-md-8">
                <h4><?= get_the_title() ?></h4>
                <?= get_the_content( '' ) ?>
            </div>
            
        </div>
        

<?php 


endforeach; 

?>
        
    </div>

<?php


else : 


?>

        <!-- No items yet -->
<?php endif; ?>


          
        

</div>




<script type="text/javascript">
    
    
    
</script>