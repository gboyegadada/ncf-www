<?php 
define('WP_USE_THEMES', false);
require('../../news/wp-blog-header.php');

?>




<div class="ajax-content-header">
    About the Author
    
</div>

<hr>

<div class="ajax-content-body">
    
    
<?php


$page = get_page_by_path('about-the-author'); 

        
$image_url = wp_get_attachment_url( get_post_thumbnail_id( $page->ID ), 'full' );
            
      ?>
    


        <!-- Carousel 
        ================================================== -->
        <div id="myCarousel" class="carousel fade col-md-6" >

          <div class="carousel-inner">


                <?php



                            $_active = true;

                                        ?>

                                        <div class="item <?php if ($_active) { echo 'active'; $_active = false; } ?>">
                                            <div class="slide-img" style="background: url('<?= $image_url ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center; ">&nbsp;</div>

                                            <div class="container">

                                            <div class="carousel-caption col-md-12">
                                                <p class="col-md-12">
                                                    <?php // print_r ($imgData); ?>
                                                </p>

                                              <div class="bg">&nbsp;</div>
                                            </div>

                                          </div>
                                        </div>






            </ul>         



        </div>

        </div>



        <div class="blurb">
                    <h4><?= $page->post_title ?></h4>
                    <?= apply_filters('the_content', $page->post_content) ?>

        </div>










</div>




<script type="text/javascript">

                // $(".ajax-content-header").stick_in_parent();

                $('.carousel').carousel({
                    interval: 5000, 
                    pause: false

                });


</script>