<?php


define ('OUTPUT', 'AJAX_HTML');
define('CONTENTS_PATH', 'contents/');






 
/**
 * inoculate against hack attempts which waste CPU cycles
 */
$contaminated = (isset($_FILES['GLOBALS']) || isset($_REQUEST['GLOBALS'])) ? true : false;
$paramsToAvoid = array('GLOBALS', '_COOKIE', '_ENV', '_FILES', '_GET', '_POST', '_REQUEST', '_SERVER', '_SESSION', 'HTTP_COOKIE_VARS', 'HTTP_ENV_VARS', 'HTTP_GET_VARS', 'HTTP_POST_VARS', 'HTTP_POST_FILES', 'HTTP_RAW_POST_DATA', 'HTTP_SERVER_VARS', 'HTTP_SESSION_VARS');
$paramsToAvoid[] = 'autoLoadConfig';
$paramsToAvoid[] = 'mosConfig_absolute_path';
$paramsToAvoid[] = 'hash';
$paramsToAvoid[] = 'main';
foreach($paramsToAvoid as $key) {
  if (isset($_GET[$key]) || isset($_POST[$key]) || isset($_COOKIE[$key])) {
    $contaminated = true;
    break;
  }
}

$paramsToCheck = array('page-id');


if (!$contaminated) {
  foreach($paramsToCheck as $key) {
    if (isset($_GET[$key]) && !is_array($_GET[$key])) {
      if (substr($_GET[$key], 0, 4) == 'http' || strstr($_GET[$key], '//')) {
        $contaminated = true;
        break;
      }
      if (isset($_GET[$key]) && strlen($_GET[$key]) > 43) {
        $contaminated = true;
        break;
      }
    }
  }
}
unset($paramsToCheck, $paramsToAvoid, $key);
if ($contaminated)
{
  header('HTTP/1.1 406 Not Acceptable');
  exit(0);
}
unset($contaminated);
/* *** END OF INNOCULATION *** */







// Check for direct calls to AJAX scripts...
if (defined('OUTPUT') && (OUTPUT == 'JSON' || OUTPUT == 'AJAX_JSON' || OUTPUT == 'AJAX_HTML')) {
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		// This an ajax request. Do nothing.
	 } else {
		header('Location: http://' . $_SERVER['HTTP_HOST']);
		exit();
	 }
}



// header('Content-type: application/x-json');



// DIE IF INPUT IS NOT VIA POST METHOD...
if ($_SERVER['REQUEST_METHOD'] != 'POST') die('x_x');
	



// FILTER VALUES ....
$_CLEAN = filter_input_array(INPUT_POST, 
                                                array(	
                                                                
                                                                'page-id'	=>	array('filter'		=>	FILTER_VALIDATE_REGEXP, 
                                                                                                'options'	=>	array('regexp' => '/^[A-Za-z0-9-]{2,4}_[A-Za-z0-9-]{1,20}_[A-Za-z0-9-]{1,200}$/') // country_city_content-id
                                                                                                                )

                                                                )
                                                );







 
 

// Check post data
 
 if (in_array(FALSE, $_CLEAN)) {

    
    echo 'x_X';

    exit ();
    
}



list($_COUNTRY, $_CITY, $_PAGE_ID) = explode('_', $_CLEAN['page-id']);

$_filename = 'contents/' . strtoupper($_COUNTRY) . '/' . strtolower($_CITY) . '/' . $_PAGE_ID . '.dat';

// OUTPUT...
if (file_exists($_filename)) echo (file_get_contents($_filename));
else echo '<h4>Content not found: ' . $_filename . '</h4>';

exit ();




?>
