<?php

/*

CLASS Twiggy 

*/



class Cataleya_Class_Twiggy 
{
	
	private static $_twig_instance;
        
	
	public static function getInstance ($_PATH = NULL, $_FORCE_INIT = FALSE) 
	{
                
		
		if (!isset(self::$_twig_instance) || $_FORCE_INIT === TRUE)
		{
                    if (!file_exists($_PATH)) $_PATH = SHARED_INC_PATH.'templates/twig';
                    
                    $loader = new Twig_Loader_Filesystem($_PATH);
                    self::$_twig_instance = new Twig_Environment($loader, array('cache'=>FALSE));
                    
                    
                    // cache_dir: SHARED_INC_PATH.'templates/twig/cache'
		
		}
		
		return self::$_twig_instance;
		
	}	
        
        
        
	public static function loadTemplate ($_template = NULL) 
	{
                if ($_template === NULL) return NULL;
		
		
		return self::getInstance()->loadTemplate($_template);
		
	}	
	
	
	
	
	
}






?>