<div class="footer-quote">
    <?php

    $idObj = get_category_by_slug('quotes'); 

    $args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 1, 'orderby' => 'rand');
    $myposts = get_posts( $args );

    if ( !empty($myposts) ) : ?>

            <?php /* The loop */


            ?>
           <?php foreach( $myposts as $post ) : setup_postdata($post); ?>

            <p class="col-md-12 pull-left">
                  <?= strip_tags(get_the_content( '' ), ''); ?>
                
            </p>
            
            <?php
            
            $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
            
            ?>
            
            <div class="quote-image" style="background: url('<?php echo $large_image_url[0]; ?>') no-repeat; background-size: contain;"></div>
            
            <?php endforeach; ?>

    <?php endif; ?>
    
    
</div>