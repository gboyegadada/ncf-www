<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<div class="footer">
    

<div class="container a">

    <div class="row">





            <div class="sponsors-wrap">
                        
                        <a id="sponsors-anchor" href="javascript: void(0);" onclick="fetch('sponsors', this);">
                        <div class="le-label">Sponsors:</div>
                        <div id="sponsors-slider" class="tile-content-wrapper" >
                            <ul class="slide-strip" >

                            <?php

                              $idObj = get_category_by_slug('sponsors'); 

                              $args = array ( 'category' => $idObj->term_id, 'posts_per_page' => 5);
                              $myposts = get_posts( $args );

                              if ( !empty($myposts) ) : ?>

                                      <?php 

                                      
                                      
                                      /* The loop */ 

                                      foreach( $myposts as $post ) : 
                                          
                                          
                                        
                                        $image_url = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ), 'medium' );
                                        if (empty($image_url)) continue;

                                                    ?>

                                                      <li class="slide" >

                                                      <div class="slide-content-wrapper" style="background: none; " >

                                                          <?php 


                                                                          echo '<div class="sponsor-logo well well-sm" style="background: #eee url(' . $image_url. ') no-repeat center center; background-size: contain;">&nbsp;</div>';


                                                          ?>

                                                      </div>

                                                      </li>



                                                      <?php
  

                                        endforeach; 
                                        
                                        else : 
                                            
                                            ?>

                                        <!-- No reviews yet -->
                                <?php endif; ?>



                            </ul>
                            </div>
                            
                            </a>


            </div>        



            <div class="footer-quote"></div>

       </div>

    </div>

            
    <hr>      

<div class="container b">

   
         
    
    <div class="row">
        
        <div id="signature" class=" col-md-5">
            Site by <a href="http://www.fancypaperplanes.com" target="_blank">Fancy Paper Planes</a> for the <span class="company"><?= COMPANY_NAME ?></span>. &copy;<?= date('Y') ?> All rights reserved.
        </div>
        
        
        <div id="social-links-wrap" class="col-md-5 pull-right">
            
            <div id="social-links" >

                    <a href="http://www.facebook.com/ThreadofGoldBeads" target="_blank"><img src="img/icon/social/faceb.gif"></a>

                    <a href="http://twitter.com/togbplay" target="_blank"><img src="img/icon/social/twit.gif"></a>

                    
                    <!--
                    <a href="http://www.youtube.com/" target="_blank"><img src="img/icon/social/youtube.png"></a>

                    <a href="http://pinterest.com/" target="_blank"><img src="img/icon/social/pinterest.png"></a>
                    <a href="http://instagram.com/" target="_blank"><img src="img/icon/social/instagram.png"></a>
                    -->
                    
            </div>
            
            <div class="beads-icon" >&nbsp;</div>
            <div class="title">Connect with <span class="togb">Thread of Gold Beads</span></div>
            
        </div>
        
            
    </div>
    
        
        

    
</div>

</div>


<br class="clearfix" />