<?php




/*
 * [ readFolder ]
 * ___________________________________________
 * 
 * Read contents of a folder and return filenames in an array.
 * 
 * 
 */


function readFolder ($_PATH = NULL, $_FILTER = '*') 
{
    if (!is_string($_FILTER) || empty($_PATH) || !is_string($_PATH) || !file_exists($_PATH)) return array ();
    
    
    $_FILTER = preg_replace('/([^a-zA-Z0-9-_]+)/i', '', $_FILTER);
    if (strlen($_FILTER) > 20) return array ();
    
    if (empty($_FILTER)) $_FILTER = '*';
    
    
    
    $_files = array ();
    $dh  = scandir($_PATH, 0);
    
    foreach ($dh as $filename) {
        if (preg_match('/^([a-zA-Z0-9-_]+)(\.' . $_FILTER . ')$/i', $filename)) $_files[] = $filename;
    }
    
    
    return  empty ($_files) ? array ('toystory.jpg') : $_files;
}







function abreviateTotalCount($value) 
{

    $abbreviations = array(12 => 'T', 9 => 'B', 6 => 'M', 3 => 'K', 0 => '');

    foreach($abbreviations as $exponent => $abbreviation) 
    {

        if($value >= pow(10, $exponent)) 
        {

            return round(floatval($value / pow(10, $exponent)), 1).$abbreviation;

        }

    }

}


?>
