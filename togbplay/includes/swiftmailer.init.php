<?php

/*

SWIFT INIT

*/


// SWIFT MAILER
require_once (SHARED_INC_PATH.'swiftmailer/lib/swift_required.php');


Swift::init(function () {
	
	Swift_DependencyContainer::getInstance()->register('mime.qpcontentencoder')->asAliasOf('mime.nativeqpcontentencoder');

});


?>