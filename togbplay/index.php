<?php 

require_once ('includes/config.php'); 
include_once 'includes/helper_functions.php';

define('WP_USE_THEMES', false);
require('news/wp-blog-header.php');

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 604, 270, true );

$_slides = readFolder('./img/slides-home/', 'jpg');


$activeLinks['home'] = 'active';


 // PAGE VIEWS

 try  {

$ga = new gapi($gaEmail, $gaPassword);

$ga->requestReportData($profileId, array('browser','browserVersion'),array('pageviews','visits'));

 $totalPageviews = $ga->getPageviews();

 } catch(Exception $e) {
     
     $totalPageviews = 0;
 
     
 }

 
 
 
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php require_once 'includes/meta.php'; ?>
        <title>Thread of Gold Beads on Stage | Home</title>
        <meta name="description" content="">

        <?php require_once 'includes/top.assets.php'; ?>
        
        
        <style>
        </style>
        
        
            

    </head>
    <body>
        
        
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        


        
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=208316102520260&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

 
        
        
        
        <div class="social-cache">
            
        


            
        </div>

      <?php // include_once 'includes/slideshow-cache.php'; ?>
        
    
      <?php require_once 'includes/header.php'; ?>





      <!-- Example row of columns -->
      
      <div class="container">
      <div class="row">

          
          <div class="main-bg col-md-12">&nbsp;</div>
          
          
          <div class="sidebar col-md-3">
              <ul>
                                
                        
                        <li class="<?= $activeLinks['our phylosophy'] ?>"><a id="home-anchor" href="javascript: void(0);" onclick="showLanding(this);">Home</a></li>
                        <li class="<?= $activeLinks['about us'] ?>"><a id="page-to-stage-anchor" href="javascript: void(0);" onclick="fetch('page-to-stage', this);">Page to Stage</a></li>
                        <li class="<?= $activeLinks['our phylosophy'] ?>"><a id="tickets-anchor" href="javascript: void(0);" onclick="fetch('tickets', this);">Get Tickets</a></li>
                        <li class="<?= $activeLinks['our phylosophy'] ?>"><a id="shop-anchor" href="javascript: void(0);" onclick="fetch('shop', this);">Shop</a></li>
                        <li class="<?= $activeLinks['our phylosophy'] ?>"><a id="news-anchor" href="javascript: void(0);" onclick="fetch('news', this);">News</a></li>
                        <li class="<?= $activeLinks['services'] ?>"><a id="casting-anchor" href="javascript: void(0);" onclick="fetch('casting', this);">Cast</a></li>
                        <li class="<?= $activeLinks['services'] ?>"><a id="gallery-anchor" href="javascript: void(0);" onclick="fetch('gallery', this);">Gallery</a></li>
                        <li class="<?= $activeLinks['services'] ?>"><a id="community-service-anchor" href="javascript: void(0);" onclick="fetch('community-service', this);">Giving Back</a></li>
                        <li class="<?= $activeLinks['services'] ?>"><a id="social-anchor" href="javascript: void(0);" onclick="fetch('social', this);">Social</a></li>

                </ul>
              
              <div class="player-control">
                  <a href="javascript:void(0);" onclick="toggleMusic();">
                      <img id="audio-icon" src="img/icon/volume-2-50x50-white.png" class="pull-left" width="15px" height="15px" />
                      &nbsp; music
                  </a>
              </div>
          </div>
          
          
          
          
          
          
          
          
          
          <div class="main-content col-md-9">
              <!-- main content -->
              
              
              
              <div class="bg">&nbsp;</div>
              
              
              
              
                <div class="ajax-content-frame black-scroller">



                <p>&nbsp;</p>

                </div>
                 
              
              
          </div>
          
          
          
          
          <div class="home-screen-wrap col-md-11">
              
              <div class="bg">&nbsp;</div>
              <div class="la-fille">&nbsp;</div>
              
              
              <div class="about-the-play">
                    <span class="togb">Thread of Gold Beads</span> written by <a id="author-anchor" href="javascript: void(0);" onclick="fetch('the-author', this);">Nike Campbell-Fatoki</a>, adapted by playwright Ava 'Sunshine' Duckett and directed by Yele Akinyelu.
                    Described by readers as: <em>"Captivating; full of suspense - a blend of history, drama and love; historical fiction at it's best, and a highly competent contribution to the growing genre of popular historical fiction in Africa;"</em>
                    
                    
                    
                    <br /><br /><br /><br />
                    <a id="tickets-anchor" class="btn btn-lg btn-primary" href="javascript: void(0);" onclick="fetch('gallery', this);">
                        <i>Link to The Premiere October 4, 2014</i>
                    </a>
                    
                    <!--
                    Watch the highly talked about historical fiction novel come alive on stage.
                    
                    
                    <div class="showing">
                        <div class="bg">&nbsp;</div>

                        <div class="date col-md-4 pull-right">
                        Show times <br />
                        October 4, 2014<br />
                        2pm and 7pm<br />
                        </div>
                        
                        <div class="venue col-md-7 pull-right">
                        Publick Playhouse for the Performing Arts <br />
                        5445 Landover Rd, Cheverly, MD 20784
                        </div>
                        
                    </div>
                    
                    <br /><br /><br /><br />
                    
                    <br /><br />
                    For Tickets, please click here 
                    <a id="tickets-anchor" class="btn btn-lg btn-primary" href="javascript: void(0);" onclick="fetch('tickets', this);">
                        Get Tickets
                    </a>
                    
                    <br /><br />
                    <div class="page-views">This page has been viewed <span class="count"><?= number_format($totalPageviews) ?><?php // abreviateTotalCount($totalPageviews) ?></span> times.</div> 
                    
                    -->
              </div>
          </div>
          
          
          
        
                

      </div>
          
      </div>
      
      

      
    <?php require_once 'includes/footer.php'; ?> 
      
      
    
    
        <?php require_once 'includes/bottom.assets.php'; ?>
      
      

    
            <script type="text/javascript">

                var sponsorsSlider;
            
                jQuery(document).ready(function ($) {


                             sponsorsSlider = makeSlideshow($('#sponsors-slider').find('.slide-strip'), {duration: 200, interval: 1000, transition: 'fade'});

                            sponsorsSlider.play(); 
                                 

                });
                
                
            </script>

    </body>
</html>
