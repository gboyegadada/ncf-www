var 

showLanding, 
fetch, 
hideContentBody, 
refreshQuote, 
getSocialFeeds, 
readMore, 
watchYoutubeVideo, 
pages = {
    home: 'home.html', 
    about: 'about.html', 
    casting: 'casting.php',  
    gallery: 'gallery.php', 
    news: 'news.php', 
    shop: 'shop.html', 
    social: 'social.php', 
    sponsors: 'sponsors.php', 
    'the-author': 'the-author.php', 
    tickets: 'tickets.html', 
    'page-to-stage': 'page-to-stage.php', 
    'community-service': 'community.php'
};

window.socialON = false;

function ge (elmt)
{
        if (window.all)
        {
        return window.all[elmt];
        } else {
        return document.getElementById(elmt);
        }


}




jQuery(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
}); 
       
       
       
jQuery(document).ready(function ($) {
    
    

            
   // $('#player2')[0].play();
            

    toggleMusic = function () {

        var player = $('#player2')[0];

        if (player.paused) {

            player.play();
            $('#audio-icon').prop('src', 'img/icon/volume-2-50x50-white.png');

        } else {

            player.pause();
            $('#audio-icon').prop('src', 'img/icon/volume-mute-50x50-white.png');
        }
    }


    
    
    showLanding = function () {
        var anchor = $('#home-anchor')[0];
        
        $('.ajax-content-frame').hide();
        $('.main-content > .bg').hide();
        $('.main-content').animate({height: '600px'}, 200);
        
        $('.home-screen-wrap').fadeIn(200);
        
        
        $('.sidebar > ul a').removeClass('active');
        $(anchor).addClass('active');
        
        refreshQuote ();
        
        location.hash = '#home';
        
        
    }
            
    fetch = function (page_id) {
        
        
                
                // If page ID is invalid GO HOME...
                if (!(page_id in pages)) {
                    showLanding();
                    return;
                }
                
                
        
                if (typeof slider01 !== 'undefined') {
                    slider01.stop();
                    slider01 = undefined;
                    
                }
                
                
                $('.home-screen-wrap').hide();
                $('.main-content').animate({height: '700px'}, 200);
                
                
                if (window.socialON) { window.socialON = false;  }
                
                
                
                
                
                var 
                
                body = $( 'body' ), 
                contentWrapper = $('.ajax-content-frame'), 
                mainContentBody = $('.profiles'), 
                url = 'ajax-content/profiles/' + pages[page_id], 
                anchor = $('#' + page_id + '-anchor')[0];
                
                location.hash = '#' + page_id;
                
                $('.main-content > .bg').fadeIn(200);
                $('.sidebar > ul a').removeClass('active');
                $(anchor).addClass('active');
                
                body.animate({
                    scrollTop: 0
                }, 300);
                
                
                mainContentBody.hide();
                
                contentWrapper
                .hide()
                .html('<center><p>&nbsp;</p><p>&nbsp;</p><img class="loading-icon blink-2" src="img/document.png" /><p>&nbsp;</p><p>&nbsp;</p></center>')
                .fadeIn(300);
                

            
                
                body.on( 'click', function (event) {
                    
                    if (! $(event.target).is('.content, .header, .footer, .wrap') ) return;
                    
                    body.off( 'click' );
                    
                    hideContentBody();
                    
                    return false
                    
                } )
                
                
                                
                $.ajax({
                            type: 'GET', 
                            url: url, 
                            dataType: 'html', 
                            success: function (data) {
                                setTimeout(function () { 
                                    
                                    contentWrapper.html(data); 
                                
                                
                                    FB.XFBML.parse(ge('ajax-content-frame'));
                                
                                
                            }, 1000);
                                
                                
                                
                                return false;
                            }
                });
                
                refreshQuote();
                
                
                
                
                
    }
    
    
    hideContentBody = function () {
        
                var 
                
                body = $( 'body' ), 
                contentWrapper = $('.ajax-content-frame'), 
                mainContentBody = $('.profiles'); 
                
                contentWrapper.fadeOut(200, function () {
                    body.off( 'click' ).animate({
                        scrollTop: $('#SCROLL-TOP').val()
                    }, 300);
                    mainContentBody.fadeIn(200);
                });
    }
    
    
    
    
    
    refreshQuote = function () {
                $('.footer-quote').fadeOut(200);
                $.get('ajax-content/profiles/quote.php', function (data, status) {
                    $('.footer-quote').html(data).fadeIn(200); 
                });
    }
    
    
    
    
    
    watchYoutubeVideo = function (id, title) {
        
        var modal = $('#MainModal');
        
        // Hide...
        modal.modal('hide');
        
        
        // Replace modal title...
        modal.find('.modal-title').html(title);
        
        // Replace embedded video...
        modal.find('.modal-body').html('<iframe width="640" height="360" src="http://www.youtube.com/embed/' + id + '" frameborder="0" allowfullscreen></iframe>');
        
        
        // Replace facebook link...
        modal.find('.fb-like').replaceWith('<div class="fb-like" data-href="http://www.youtube.com/' + id + '" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>');
        FB.XFBML.parse($('#MainModal')[0]);
        
        
        // Show...
        modal.modal('show');
    }
    
    
    
    
    
    getSocialFeeds = function (pixel) {
        $(pixel).remove();
        
        
        
        if (!window.socialON) {
            
            if (typeof twttr !== 'undefined') twttr.widgets.load();
            FB.XFBML.parse(ge('ajax-content-frame'));
            
            window.socialON = true;
        }
       
       
    }
    
    
    readMore = function () {
        
        if ($('.actor-du-jour .long').is(':hidden')) {
            $('.actor-du-jour .short').hide();
            $('.actor-du-jour .long').show(200);
            
            $('.actor-du-jour .expand-button').html('Collapse');
        } else {
            $('.actor-du-jour .long').hide();
            $('.actor-du-jour .short').show(200);
            
            $('.actor-du-jour .expand-button').html('Read more');
        }
    }
    
    
    
    $('.modal').bind('hide', function () {
            var iframe = $(this).children('div.modal-body').find('iframe'); 
            var src = iframe.attr('src');
            iframe.attr('src', '');
            iframe.attr('src', src);
    });
    
    
    
    var key = location.hash.substring(1);
    if (key in pages && key != 'home') fetch(key);

    else  showLanding(); // fetch('home.html', $('#home-anchor')[0]);

    
    
    
  
});




