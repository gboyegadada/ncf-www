<?php 

require_once ('includes/config.php'); 
include_once 'includes/helper_functions.php';

define('WP_USE_THEMES', false);
require('news/wp-blog-header.php');

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 604, 270, true );

$_slides = readFolder('./img/slides-home/', 'jpg');


$activeLinks['home'] = 'active';

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php require_once 'includes/meta.php'; ?>
        <title>Thread of Gold Beads on Stage | Home</title>
        <meta name="description" content="">

        <?php require_once 'includes/top.assets.php'; ?>
        
        
        <style>
        </style>
        
        
            

    </head>
    <body>
        
        
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        


        
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=208316102520260&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

 
        
        
        
        <div class="social-cache">
            
        


            
        </div>

      <?php // include_once 'includes/slideshow-cache.php'; ?>
        
    
      <?php require_once 'includes/header.php'; ?>





      <!-- Example row of columns -->
      
      <div class="container">
      <div class="row">
          
          
          <!--
          <div class="col-md-12 progress-bar-outer-wrap">
                <div id="progress_bar_container" style="display:none; ">

                    <div id="progress_bar" style="height:0px; ">&nbsp;</div>
                    <div id="progress_bar_bg">&nbsp;</div>

                </div>
          </div>
          -->
          
          
          <div class="main-bg col-md-12">&nbsp;</div>
          
          
          <div class="sidebar col-md-3">
              <ul>
                                
                        
                        <li class="<?= $activeLinks['our phylosophy'] ?>"><a id="home-anchor" href="javascript: void(0);" onclick="fetch('home.html', this);">Home</a></li>
                        <li class="<?= $activeLinks['about us'] ?>"><a href="javascript: void(0);" onclick="fetch('page-to-stage.html', this);">Page to Stage</a></li>
                        <li class="<?= $activeLinks['our phylosophy'] ?>"><a href="javascript: void(0);" onclick="fetch('tickets.html', this);">Get Tickets</a></li>
                        <li class="<?= $activeLinks['our phylosophy'] ?>"><a href="javascript: void(0);" onclick="fetch('shop.html', this);">Shop</a></li>
                        <li class="<?= $activeLinks['our phylosophy'] ?>"><a href="javascript: void(0);" onclick="fetch('news.php', this);">News</a></li>
                        <li class="<?= $activeLinks['services'] ?>"><a href="javascript: void(0);" onclick="fetch('casting.html', this);">Casting Call</a></li>
                        <li class="<?= $activeLinks['services'] ?>"><a href="javascript: void(0);" onclick="fetch('social.php', this);">Social</a></li>

                </ul>
          </div>
          
          
          
          
          
          
          
          
          
          <div class="main-content col-md-9">
              <!-- main content -->
              
              
              
              <div class="bg">&nbsp;</div>
              
              
              
              
                <div class="ajax-content-frame black-scroller">



                <p>&nbsp;</p>

                </div>
                 
              
              
          </div>
          
          
          
        
                

      </div>
          
      </div>
      
      

      
    <?php require_once 'includes/footer.php'; ?> 
      
      
    
    
        <?php require_once 'includes/bottom.assets.php'; ?>
    
            <script type="text/javascript">





            </script>

    </body>
</html>
